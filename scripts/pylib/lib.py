import csv
import os

def parseCsv(filePath, delimiter):
    args = []
    values = []
    values2 = []
    with open(filePath) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        lc = 0
        for row in csv_reader:
            if lc == 0:
                pass
            else:
                args.append(row[0])
                values.append(round(float(row[1]), 2))
                values2.append(round(float(row[2]), 2))
            lc+=1
    return (args, values, values2)

def resolve_title(file_path : str):
    file_name_ext = os.path.basename(file_path)
    file_name = os.path.splitext(file_name_ext)[0]
    words = file_name.split("_")

    word_map = {
        "matmult" : "Mnożenie macierz-macierz",
        "matvec" : "Mnożenie macierz-wektor",
        "diffstr" : "Różne struktury danych",
        "threads" : "Różna ilość wątków"
    }

    title = ""
    for word in words:
        if word in word_map:
            title += word_map[word]
        else:
            if int(word):
                title += ("Wymiar " + word)
            else:
                title += word
        title += " | "
    title = title[:-3]

    return title