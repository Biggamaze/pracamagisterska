import argparse
import matplotlib.pyplot as plt
import pylib.lib as lib
import os
import numpy as np

def main():
    args = parseArgs()
    data = lib.parseCsv(args.csv, args.delimiter)
    fileToSave = os.path.splitext(args.csv)[0] + ".png"
    plot(data, args.csvType, fileToSave, args.ylabel)

def parseArgs():
    parser = argparse.ArgumentParser(description="Plotting script for CSV reports")
    parser.add_argument("csv", help="Path to delimited csv file.", type=str)
    parser.add_argument("delimiter", help="Delimiter for csv file", type=str)
    parser.add_argument("csvType", help="hist or graph", type=str ,choices=["hist","graph","ch"])
    parser.add_argument("--y", dest="ylabel", type=str)
    return parser.parse_args()

def plot(data, csvType, fileToSave, ylabel):
    if csvType == "graph":
        plt.plot(data[0], data[1], "b*")
        plt.xlabel("Ilość wątków [-]")
        # plt.ylabel("Przyspieszenie [-]")
        # plt.ylabel("Czas wykonania [ms]")
        plt.ylabel(ylabel)
        plt.grid(b=True, which='both', axis='both', linestyle='-')
        plt.title(lib.resolve_title(fileToSave))
        plt.savefig(fileToSave)
    elif csvType == "hist":
        y_pos = np.arange(len(data[0]))
        plt.barh(y_pos, data[1], align='center', alpha=0.7)
        plt.yticks(y_pos, data[0])
        plt.xlabel("Czas wykonania [s]")
        plt.title(lib.resolve_title(fileToSave))
        plt.grid(b=True, which='both', axis='x', linestyle='-')
        plt.savefig(fileToSave, bbox_inches='tight')
    elif csvType == "ch":
        plt.plot(data[0], data[1], "b*", label="Haskell")
        plt.plot(data[0], data[2], "b+", label="C")
        plt.xlabel("Ilość wątków [-]")
        plt.ylim(0)
        # plt.ylabel("Przyspieszenie [-]")
        plt.legend()
        plt.ylabel(ylabel)
        plt.grid(b=True, which='both', axis='both', linestyle='-')
        plt.title("Macierz-wektor | Różna ilość wątków | Rozmiar 10000")
        plt.savefig(fileToSave)
    else:
        pass
        

if __name__ == "__main__":
    main()