#!/usr/bin/zsh

printf "--- SETUP AND PREQS ---\n\n"

current_work_dir=$(pwd)
script_root_path=$(dirname $0)
printf "Script root relative path: %s\n" $script_root_path
printf "Setting working directory to repository root ...\n"
cd "$script_root_path/.."
repo_root=$(pwd)
printf "Working directory: %s\n" $repo_root
printf "Setting up folder structure\n"
$repo_root/scripts/setup.sh
reports_folder=$repo_root/reports/c

source $repo_root/scripts/pyenv/bin/activate

# Lets do env check
# Required programs
require_program () {
    printf "Checking for existance of program %s ... " $1
    if (( $+commands[$1] )); then
        printf "Program found\n"
    else
        printf "Not found! Please install %s\n" $1
        exit 1
    fi
}
require_program gcc
require_program make

# Number of cores
hyper_threaded=$(grep -c ^processor /proc/cpuinfo)
physical_cores=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')

if [ $hyper_threaded -eq $physical_cores ]
then
    printf "Using machine without hyperthreading. Physical cores: %d\n" $physical_cores
    omp_max_threads=$physical_cores
else
    printf "Using machine with hyperthreading. Physical cores: %d, logical cores: %d\n" $physical_cores $hyper_threaded
    omp_max_threads=$hyper_threaded
fi

# Compile programs
printf "\n--- COMPLITATION ---\n\n"
export ISPROD='-D PROD'
makefile_dir="$repo_root/src/c_code"
printf "Entering $makefile_dir\n"
cd $makefile_dir
printf "Compliling matmult and matvec\n"
make clean
make matmult
if [ $? -ne 0 ] ; then
    printf "Failed compliation. Exiting\n"
    exit 1
fi
make matvec
if [ $? -ne 0 ] ; then
    printf "Failed compliation. Exiting\n"
    exit 1
fi
printf "Compiled succesfully\n"

#run tests
printf "\n--- MEASUREMENT ---\n\n"
printf "Executing test for maximum number of threads: $omp_max_threads\n"
now=$(date +%d_%m_%y__%H_%M_%S)

current_report_folder=$reports_folder/$now
mkdir -p "$current_report_folder/data"
mkdir -p "$current_report_folder/img"

mv_file="$current_report_folder/data/matvec_threads_"
mm_file="$current_report_folder/data/matmult_threads_"

printf "\n--- MATVEC ---\n\n"

dimensions=(10000)
bench_count=30

for dimension in $dimensions ; do
    
    printf "%s Executing sequential algorithm for size %d\n" "$(date)" $dimension 
    
    mv=0
    for time in {1..$bench_count}; do
        (( mv=$($repo_root/bin/c_bin/matvec $dimension --seq) + $mv ))
    done
    
    (( mv = mv/$bench_count ))
    
    printf "Wątki;Czas [ms]\n" > "${mv_file}$dimension"
    printf "1;$mv\n" >> "${mv_file}$dimension"
    
    for cores in {2..$omp_max_threads}; do
        
        export OMP_NUM_THREADS=$cores
        printf "%s Executing for size %d on %d threads\n" "$(date)" $dimension $cores
        
        mv=0
        for time in {1..$bench_count}; do
           (( mv=$($repo_root/bin/c_bin/matvec $dimension --par) + $mv ))
        done
        
        (( mv=$mv/$bench_count ))

        printf "$cores;$mv\n" >> "${mv_file}$dimension"
    done

python3 $repo_root/scripts/plot.py "${mv_file}$dimension" ';' 'graph'

done

printf "\n--- MATMULT ---\n\n"

dimensions=(2000)
bench_count=25

for dimension in $dimensions ; do
    
    printf "%s Executing sequential algorithm for size %d\n" "$(date)" $dimension
    
    mm=0
    for time in {1..$bench_count}; do
        (( mm=$(nice -n -20 $repo_root/bin/c_bin/matmult $dimension --seq) + $mm ))
    done
    
    (( mm = mm/$bench_count ))
    
    printf "Wątki;Czas [ms]\n" > "${mm_file}$dimension"
    printf "1;$mm\n" >> "${mm_file}$dimension"
    
    for cores in {2..$omp_max_threads}; do
        
        export OMP_NUM_THREADS=$cores
        printf "%s Executing for size %d on %d threads\n" "$(date)" $dimension $cores
        
        mm=0
        for time in {1..$bench_count}; do
           (( mm=$($repo_root/bin/c_bin/matmult $dimension --par) + $mm ))
        done
        
        (( mm=$mm/$bench_count ))

        printf "$cores;$mm\n" >> "${mm_file}$dimension"
    done

python3 $repo_root/scripts/plot.py "${mm_file}$dimension" ';' 'graph'

done

printf "\n--- CLEANUP ---\n\n"

printf "%s Restoring previous working directory\n" "$(date)"
cd $current_work_dir