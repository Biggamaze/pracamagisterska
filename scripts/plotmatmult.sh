script_root_path=$(dirname $0)

echo $script_root_path

source $script_root_path/pyenv/bin/activate

python3 $script_root_path/plot.py "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/matmult_diffstr_500.csv" "," "hist" 

python3 $script_root_path/plot.py "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/matmult_diffstr_1000.csv" "," "hist" 

python3 $script_root_path/plot.py "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/threads_time/matmult_threads_10000.csv" "," "graph" --y "Czas wykonania [s]"
mv "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/threads_time/matmult_threads_10000.png" \
"/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/threads_time/matmult_threads_10000_time.png"

python3 $script_root_path/plot.py "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/threads_speedup/matmult_threads_10000.csv" "," "graph" --y "Przyspieszenie [-]"
mv "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/threads_speedup/matmult_threads_10000.png" \
"/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/threads_speedup/matmult_threads_10000_speedup.png"

python3 $script_root_path/plot.py "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/cvh_time/matmult_threads_10000.csv" "," "ch" --y "Czas wykonania [s]"
mv "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/cvh_time/matmult_threads_10000.png" \
"/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/cvh_time/matmult_threads_10000_time.png"

python3 $script_root_path/plot.py "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/cvh_speedup/matmult_threads_10000.csv" "," "graph" --y "Czas Haskell / Czas C [-]"
mv "/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/cvh_speedup/matmult_threads_10000.png" \
"/home/radoslaw/Projects/pracamagisterska/saved_reports/haskell/03_06_19__21_00_43/matmult/edited/cvh_speedup/matmult_threads_10000_speedup.png"
