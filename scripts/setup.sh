#!/usr/bin/zsh

script_path=$(dirname $0)
repo_root=$script_path/..

c_bin=${script_path}/../bin/c_bin
if [ ! -d $c_bin ]
then
    mkdir -p $c_bin
    printf "Created directory %s" $c_bin
fi

report_folder=$repo_root/reports/c
if [ ! -d $report_folder ]
then
    mkdir -p $report_folder
    printf "Created directory %s" $report_folder
fi

report_folder=$repo_root/reports/haskell
if [ ! -d $report_folder ]
then
    mkdir -p $report_folder
    printf "Created directory %s" $report_folder
fi

