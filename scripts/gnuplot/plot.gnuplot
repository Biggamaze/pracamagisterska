# command line arguments
# e.g gnuplot -e "TITLE='edlo';XAXIS='xaxis'" my_plotfile
if (!exists("TITLE")) TITLE='<title placeholder>'
if (!exists("XAXIS")) XAXIS='<xaxis placeholder>'
if (!exists("YAXIS")) YAXIS='<yaxis placeholder>'
if (!exists("XMAX")) XMAX=13
if (!exists("FILENAME")) FILENAME='data.txt'
if (!exists("OUTPUT")) OUTPUT='obrazek.png'

# description
set title TITLE
set xlabel XAXIS
set ylabel YAXIS

# ustalenie skali logarytmicznej dla osi x i y
#set logscale y
#set logscale x
set autoscale
set yrange [0:]
set xrange [0:XMAX]

# ustalenie stylu uzywanych linii 
#set style line 1 lt 1
#set style line 2 lt 2

# ustalenie miejsc pojawiania sie znaczkow na osiach
set xtics 1
set mxtics 10
set mytics 10

# ustalenie wygladu siatki pojawiajacej sie na wykresie
#set grid mxtics mytics xtics ytics
set grid xtics ytics

# ustalenie miejsca pojawienia sie legendy
set key top left

set output OUTPUT
set terminal png size 800,600

F250 = sprintf("%s%s", FILENAME, "250")
F1000 = sprintf("%s%s", FILENAME, "1000")
F2000 = sprintf("%s%s", FILENAME, "2000")

# [0:YMAX][0:YMAX]
plot F250 title "Wymiar 250" with linespoints linetype 2 linewidth 1 pointtype 7 pointsize 2 \
, F1000 title "Wymiar 1000" with linespoints linetype 1 linewidth 1 pointtype 7 pointsize 2 \
, F2000 title "Wymiar 2000" with linespoints linetype 3 linewidth 1 pointtype 7 pointsize 2 \

# pause -1 "Press anything to continue"