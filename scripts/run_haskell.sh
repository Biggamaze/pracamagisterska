#!/usr/bin/zsh

printf "--- SETUP AND PREQS ---\n\n"

# SETTING PATHS
initial_work_dir=$(pwd)
script_root_path=$(dirname $0)
cd $script_root_path/..
repo_root=$(pwd)
haskell_root=$repo_root/src/haskell_code


# SETUP
$repo_root/scripts/setup.sh
reports_folder=$repo_root/reports/haskell

now=$(date +%d_%m_%y__%H_%M_%S)
current_report_folder=$reports_folder/$now
mkdir -p $current_report_folder

printf "--- PREQUISITES CHECK ---\n\n"

require_program () {
    printf "Checking for existance of program %s ... " $1
    if (( $+commands[$1] )); then
        printf "Program found\n"
    else
        printf "Not found! Please install %s\n" $1
        exit 1
    fi
}
require_program stack

printf "--- ENVIRONMENT INFORMATION ---\n\n"

# Number of cores
hyper_threaded=$(grep -c ^processor /proc/cpuinfo)
physical_cores=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')

if [ $hyper_threaded -eq $physical_cores ]
then
    printf "Using machine without hyperthreading. Physical cores: %d\n" $physical_cores
    omp_max_threads=$physical_cores
else
    printf "Using machine with hyperthreading. Physical cores: %d, logical cores: %d\n" $physical_cores $hyper_threaded
    omp_max_threads=$hyper_threaded
fi

printf "\n--- RUN BENCHMARKS ---\n\n"

cd $haskell_root

matvec_report_folder=$current_report_folder/matvec
mkdir -p $matvec_report_folder

matmult_report_folder=$current_report_folder/matmult
mkdir -p $matmult_report_folder

printf "--- RUNNING MATVEC ---\n"

# sizes for DifferentStructures both matmult and matvec
dimensions=(500 1000 5000)

for dim in $dimensions; do

    export MASTERDEG_HASKELL_DIMENSIONS="$dim"

    printf "Running for dimension $dim\n"

    stack bench masterdeg:matvec-diffstr \
    --ba="--csv $matvec_report_folder/matvec_diffstr$dim.csv --output=$matvec_report_folder/matvec_diffstr_$dim.html" \
    2>> $matvec_report_folder/matvec_diffstr_output.txt

    if [ $? -ne 0 ] ; then
        printf "Failed matvec-diffstr benchmarks. Exiting\n"
        exit 1
    fi

done

# size for Threads both matmult and matvec
export THREADS_BENCH_DIMENSION=10000

stack bench masterdeg:matvec-threads \
--ba="--csv $matvec_report_folder/matvec_threads.csv --output=$matvec_report_folder/matvec_threads_$THREADS_BENCH_DIMENSION.html" \
2> $matvec_report_folder/matvec_threads_output.txt

if [ $? -ne 0 ] ; then
    printf "Failed matvec-threads benchmarks. Exiting\n"
    exit 1
fi

printf "--- RUNNING MATMULT ---\n"

dimensions=(500 1000)

for dim in $dimensions ; do

    export MASTERDEG_HASKELL_DIMENSIONS=$dim

    stack bench masterdeg:matmult-diffstr \
    --ba="--csv $matmult_report_folder/matmult_diffstr$dim.csv --output=$matmult_report_folder/matmult_diffstr_$dim.html" \
    2> $matmult_report_folder/matmult_diffstr_output.txt

    if [ $? -ne 0 ] ; then
        printf "Failed matmult-diffstr benchmarks. Exiting\n"
        exit 1
    fi
done

# size for Threads both matmult and matvec
export THREADS_BENCH_DIMENSION=2000

stack bench masterdeg:matmult-threads \
--ba="--csv $matmult_report_folder/matmult_threads.csv --output=$matmult_report_folder/matmult_threads_$THREADS_BENCH_DIMENSION.html" \
2> $matmult_report_folder/matmult_threads_output.txt

if [ $? -ne 0 ] ; then
    printf "Failed matmult-threads benchmarks. Exiting\n"
    exit 1
fi

printf "--- CLEANUP ---\n\n"

cd $initial_work_dir