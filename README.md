# Repozytorium pracy magisterskiej
## Radosław Kot

## Zawartość repozytorium
- kod Haskella w folderze src/haskell_code (obecnie to priorytet)
- kod C do porównania wyników (obecnie drugorzędny) w folderze src/c_code
- skrypty do automatyacji (trzeciorzędne)

## Jak uruchomić?

### `stack` 
Narzędzie do obsługi pakietów oraz build system dla Haskella. Można je pobrać ze strony https://docs.haskellstack.org/en/stable/README/#how-to-install

Aby zbudować wystarczy potem w linii komend wpisać `stack build masterdeg` a potem aby uruchomić `stack exec masterdeg`.
Możliwe jest także odpalenie benchmarków `stack bench masterdeg:matvec-benchmarks` (masterdeg to nazwa paczki z pliku cabal, a matvec-benchmark to nazwa benchmarku).

UWAGA! Każda z tych komend spowoduje pobranie sporej ilości danych, ich ostateczna wielkość może osiągnąć nawet 6GB.

Aby benchmark przebiegał prawidłowo (tzn. na wielu rdzeniach) przed uruchomienie wpisać w konsoli `export GHCRTS='-N'`.

Możliwe problemy z kompilacją:
- brak odpowieniej wersji `llvm` - pobrać clanga 6.0 i mieć nadzieję, że pomoże. Jak nie - kombinować.

### `docker`
Powstało repozytorium na DockerHubie pod adresem https://cloud.docker.com/u/graycatto/repository/docker/graycatto/masterdeg

`docker run graycatto/masterdeg:<tag> -p 9080-9081:9080-9081`

Tagi w repozytorium:

- alpha - pierwszy działający, serwer WebSocket nie odpowiada
- beta - WebSockety potencjalnie odpowiadają

Ten sposób uruchomienia nie daje dostępu do testów ani benchmarków, jest jednak zdecydowanie prostszy.


Należy też przemapować porty przy starcie kontenera `-p 9080-9081:9080-9081`.

### Część wspólna
Domyślnym portem serwera REST jest port 9080 a servera WebSocket 9081 (lub REST+1 jeśli port ten jest zmieniany z linii komend).

Dokumentacji na razie brak. Endpointy można wyczytać z kodu w pliku Server.hs. Z WebSocketami wystarczy się połączyć, zaleją klienta logami same :)

Pewną pomocą mogą być zawarte w folderze misc (w katalogu głównym) kolekcja requestów z Postmana wraz ze zmiennymi środowiskowymi.


## Folder Haskella (src/haskell_code/)
- zawiera pliki masterdeg.cabal, stack.yaml potrzebne do działania narzędzia `stack`
- Dockerfile 
- w folderze `src/Algorithm`:
  
  -  `/Concurrent`:
    
     - dwie implementacje Pubu - opartą na MVar'ach (Pub.hs -- zła implementacja) oraz na TVarach (Software Transactional Memory, PubSTM.hs)

  - `/Parallel`

     - całkowanie sekwencyjne i równoległe - Integration.hs
     - obliczanie ciągu Fibonacciego - Fibonacci.hs
     - Matmult.hs, Matvec.hs - sztandarowe algorytmy (Matmult może nie dziłać póki co)
     
- w folderze `app`

  - Main.hs - punkt wejściowy do aplikacji - startuje sewer REST oraz WebSocket
  - Server.hs - serwer REST
  - WebSocketServer.hs - służy do zabawy z PubSTM :)

- w folderze `benchmark`

  - dwa pliki z benchmarkami funkcji z plików o odpowiednich nazwach
  - póki co można je odpalić tylko z kodu źródłowego
  - wykorzystują bibliotekę (framework ?) `criterion`

- w folderze `test`

  - testy jednostkowe - HUnit oraz HSpec

