masterdeg-0.1.0.0: unregistering (components added: bench:matvec-threads)
Progress 0/2            masterdeg-0.1.0.0: build (lib + bench)
Progress 0/2            Preprocessing library for masterdeg-0.1.0.0..
Progress 0/2            Building library for masterdeg-0.1.0.0..
Progress 0/2            Preprocessing benchmark 'matvec-threads' for masterdeg-0.1.0.0..
Progress 0/2            Building benchmark 'matvec-threads' for masterdeg-0.1.0.0..
Progress 0/2            [1 of 1] Compiling Main             ( benchmark/Matvec/Threads.hs, .stack-work/dist/x86_64-linux/Cabal-2.4.0.1/build/matvec-threads/matvec-threads-tmp/Main.o ) [Utils.DataGeneration changed]
Progress 0/2            Linking .stack-work/dist/x86_64-linux/Cabal-2.4.0.1/build/matvec-threads/matvec-threads ...
Progress 0/2            masterdeg-0.1.0.0: copy/register
Progress 0/2            Installing library in /home/radoslaw/Projects/pracamagisterska/src/haskell_code/.stack-work/install/x86_64-linux/lts-13.19/8.6.4/lib/x86_64-linux-ghc-8.6.4/masterdeg-0.1.0.0-5gPkBV7K34OFx04ifqVN7D
Progress 0/2            Installing executable masterdeg in /home/radoslaw/Projects/pracamagisterska/src/haskell_code/.stack-work/install/x86_64-linux/lts-13.19/8.6.4/bin
Progress 0/2            Installing executable temp in /home/radoslaw/Projects/pracamagisterska/src/haskell_code/.stack-work/install/x86_64-linux/lts-13.19/8.6.4/bin
Progress 0/2            Registering library for masterdeg-0.1.0.0..
Progress 0/2            masterdeg-0.1.0.0: benchmarks
Progress 0/2            Progress 1/2: masterdeg-0.1.0.0                               Running 1 benchmarks...
Progress 1/2: masterdeg-0.1.0.0                               Benchmark matvec-threads: RUNNING...
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec sequential/ dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 369.4 ms   (369.3 ms .. 369.4 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 369.3 ms   (369.2 ms .. 369.3 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              55.20 μs   (893.4 ns .. 67.76 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 19% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 2/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 219.9 ms   (219.1 ms .. 220.4 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 219.9 ms   (219.6 ms .. 220.1 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              380.9 μs   (270.5 μs .. 495.3 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 14% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 3/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 169.4 ms   (168.5 ms .. 170.3 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 169.0 ms   (168.8 ms .. 169.3 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              364.4 μs   (291.2 μs .. 428.2 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 12% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 4/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 143.1 ms   (139.7 ms .. 145.8 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 145.9 ms   (144.4 ms .. 149.2 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              3.082 ms   (1.398 ms .. 4.406 ms)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 12% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 5/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 131.0 ms   (127.9 ms .. 133.2 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.999 R²   (0.998 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 133.1 ms   (132.2 ms .. 134.9 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              1.822 ms   (946.5 μs .. 2.670 ms)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 11% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 6/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 125.8 ms   (123.8 ms .. 127.4 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 124.0 ms   (123.4 ms .. 124.9 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              1.200 ms   (892.6 μs .. 1.661 ms)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 11% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 7/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 119.4 ms   (118.7 ms .. 120.4 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 119.5 ms   (119.2 ms .. 119.9 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              475.8 μs   (301.2 μs .. 630.6 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 11% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 8/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 114.5 ms   (114.0 ms .. 115.1 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 113.9 ms   (113.4 ms .. 114.1 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              514.2 μs   (249.8 μs .. 747.9 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 11% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 9/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 111.4 ms   (109.5 ms .. 113.0 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 111.7 ms   (110.9 ms .. 112.3 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              950.4 μs   (615.0 μs .. 1.324 ms)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 11% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 10/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 109.4 ms   (108.6 ms .. 110.8 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 107.7 ms   (107.1 ms .. 108.3 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              1.052 ms   (814.4 μs .. 1.414 ms)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 11/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 106.0 ms   (105.0 ms .. 106.5 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (1.000 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 106.3 ms   (105.9 ms .. 106.6 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              619.5 μs   (395.5 μs .. 974.2 μs)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking matvec par/ threads: 12/dimension: 10000
Progress 1/2: masterdeg-0.1.0.0                               time                 104.5 ms   (102.9 ms .. 105.7 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 104.2 ms   (103.8 ms .. 104.7 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              717.5 μs   (537.4 μs .. 1.016 ms)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               Benchmark matvec-threads: FINISH
Progress 1/2: masterdeg-0.1.0.0                               Completed 2 action(s).
