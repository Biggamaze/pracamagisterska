Progress 0/2            masterdeg-0.1.0.0: build (lib + bench)
Progress 0/2            Preprocessing library for masterdeg-0.1.0.0..
Progress 0/2            Building library for masterdeg-0.1.0.0..
Progress 0/2            Preprocessing benchmark 'matvec-diffstr' for masterdeg-0.1.0.0..
Progress 0/2            Building benchmark 'matvec-diffstr' for masterdeg-0.1.0.0..
Progress 0/2            [1 of 1] Compiling Main             ( benchmark/Matvec/DifferentStructures.hs, .stack-work/dist/x86_64-linux/Cabal-2.4.0.1/build/matvec-diffstr/matvec-diffstr-tmp/Main.o ) [Utils.Helpers changed]
Progress 0/2            Linking .stack-work/dist/x86_64-linux/Cabal-2.4.0.1/build/matvec-diffstr/matvec-diffstr ...
Progress 0/2            masterdeg-0.1.0.0: copy/register
Progress 0/2            Installing library in /home/radoslaw/Projects/pracamagisterska/src/haskell_code/.stack-work/install/x86_64-linux/lts-13.19/8.6.4/lib/x86_64-linux-ghc-8.6.4/masterdeg-0.1.0.0-5gPkBV7K34OFx04ifqVN7D
Progress 0/2            Installing executable masterdeg in /home/radoslaw/Projects/pracamagisterska/src/haskell_code/.stack-work/install/x86_64-linux/lts-13.19/8.6.4/bin
Progress 0/2            Installing executable temp in /home/radoslaw/Projects/pracamagisterska/src/haskell_code/.stack-work/install/x86_64-linux/lts-13.19/8.6.4/bin
Progress 0/2            Registering library for masterdeg-0.1.0.0..
Progress 0/2            masterdeg-0.1.0.0: benchmarks
Progress 0/2            Progress 1/2: masterdeg-0.1.0.0                               Running 1 benchmarks...
Progress 1/2: masterdeg-0.1.0.0                               Benchmark matvec-diffstr: RUNNING...
Progress 1/2: masterdeg-0.1.0.0                               benchmarking list of lists : 500/sequential
Progress 1/2: masterdeg-0.1.0.0                               time                 1.262 ms   (1.251 ms .. 1.275 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.999 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.256 ms   (1.252 ms .. 1.265 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              19.71 μs   (11.11 μs .. 37.18 μs)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking list of lists : 500/parallel_par
Progress 1/2: masterdeg-0.1.0.0                               time                 1.342 ms   (1.327 ms .. 1.358 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.998 R²   (0.996 R² .. 0.999 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.336 ms   (1.320 ms .. 1.358 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              73.97 μs   (60.64 μs .. 95.74 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 43% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking list of lists : 500/parallel_strategy
Progress 1/2: masterdeg-0.1.0.0                               time                 1.250 ms   (1.224 ms .. 1.282 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.995 R²   (0.991 R² .. 0.998 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.268 ms   (1.249 ms .. 1.300 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              90.00 μs   (61.87 μs .. 119.5 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 56% (severely inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking array boxed: 500/sequential
Progress 1/2: masterdeg-0.1.0.0                               time                 2.131 ms   (2.028 ms .. 2.206 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.993 R²   (0.990 R² .. 0.997 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 2.151 ms   (2.111 ms .. 2.202 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              150.0 μs   (117.1 μs .. 186.7 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 50% (severely inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking array boxed: 500/parallel
Progress 1/2: masterdeg-0.1.0.0                               time                 1.915 ms   (1.892 ms .. 1.940 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.998 R²   (0.996 R² .. 0.999 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.951 ms   (1.921 ms .. 2.006 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              140.7 μs   (83.10 μs .. 228.7 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 54% (severely inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking array unboxed: 500/sequential
Progress 1/2: masterdeg-0.1.0.0                               time                 1.148 ms   (1.143 ms .. 1.150 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.171 ms   (1.158 ms .. 1.206 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              65.38 μs   (14.54 μs .. 117.4 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 44% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking array unboxed: 500/parallel
Progress 1/2: masterdeg-0.1.0.0                               time                 2.755 ms   (2.515 ms .. 2.991 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.949 R²   (0.923 R² .. 0.967 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 2.574 ms   (2.420 ms .. 2.710 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              470.3 μs   (405.8 μs .. 566.4 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 88% (severely inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking vector boxed : 500/sequential
Progress 1/2: masterdeg-0.1.0.0                               time                 1.059 ms   (1.054 ms .. 1.066 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.059 ms   (1.056 ms .. 1.064 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              12.95 μs   (2.576 μs .. 20.63 μs)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking vector boxed : 500/parallel
Progress 1/2: masterdeg-0.1.0.0                               time                 1.094 ms   (1.083 ms .. 1.108 ms)
Progress 1/2: masterdeg-0.1.0.0                                                    0.999 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 1.099 ms   (1.092 ms .. 1.109 ms)
Progress 1/2: masterdeg-0.1.0.0                               std dev              26.56 μs   (17.08 μs .. 42.58 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 14% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking vector unboxed : 500/sequential
Progress 1/2: masterdeg-0.1.0.0                               time                 915.8 μs   (912.0 μs .. 922.9 μs)
Progress 1/2: masterdeg-0.1.0.0                                                    1.000 R²   (0.999 R² .. 1.000 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 912.9 μs   (911.8 μs .. 917.0 μs)
Progress 1/2: masterdeg-0.1.0.0                               std dev              6.682 μs   (1.568 μs .. 13.93 μs)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               benchmarking vector unboxed : 500/parallel
Progress 1/2: masterdeg-0.1.0.0                               time                 937.8 μs   (924.6 μs .. 952.9 μs)
Progress 1/2: masterdeg-0.1.0.0                                                    0.998 R²   (0.996 R² .. 0.999 R²)
Progress 1/2: masterdeg-0.1.0.0                               mean                 951.8 μs   (942.7 μs .. 969.7 μs)
Progress 1/2: masterdeg-0.1.0.0                               std dev              45.08 μs   (31.51 μs .. 70.50 μs)
Progress 1/2: masterdeg-0.1.0.0                               variance introduced by outliers: 37% (moderately inflated)
Progress 1/2: masterdeg-0.1.0.0                               
Progress 1/2: masterdeg-0.1.0.0                               Benchmark matvec-diffstr: FINISH
Progress 1/2: masterdeg-0.1.0.0                               Completed 2 action(s).
masterdeg-0.1.0.0: benchmarks
Running 1 benchmarks...
Benchmark matvec-diffstr: RUNNING...
benchmarking list of lists : 1000/sequential
time                 5.054 ms   (5.033 ms .. 5.088 ms)
                     1.000 R²   (0.999 R² .. 1.000 R²)
mean                 5.054 ms   (5.043 ms .. 5.079 ms)
std dev              54.71 μs   (31.82 μs .. 82.52 μs)

benchmarking list of lists : 1000/parallel_par
time                 4.984 ms   (4.920 ms .. 5.031 ms)
                     0.999 R²   (0.998 R² .. 1.000 R²)
mean                 4.749 ms   (4.697 ms .. 4.802 ms)
std dev              177.4 μs   (144.2 μs .. 227.3 μs)
variance introduced by outliers: 18% (moderately inflated)

benchmarking list of lists : 1000/parallel_strategy
time                 4.288 ms   (4.189 ms .. 4.369 ms)
                     0.997 R²   (0.997 R² .. 0.998 R²)
mean                 4.030 ms   (3.980 ms .. 4.075 ms)
std dev              168.3 μs   (138.1 μs .. 199.0 μs)
variance introduced by outliers: 22% (moderately inflated)

benchmarking array boxed: 1000/sequential
time                 8.553 ms   (8.273 ms .. 8.926 ms)
                     0.992 R²   (0.985 R² .. 1.000 R²)
mean                 8.465 ms   (8.368 ms .. 8.668 ms)
std dev              329.5 μs   (177.8 μs .. 518.3 μs)
variance introduced by outliers: 17% (moderately inflated)

benchmarking array boxed: 1000/parallel
time                 6.129 ms   (5.951 ms .. 6.328 ms)
                     0.994 R²   (0.990 R² .. 0.997 R²)
mean                 6.398 ms   (6.265 ms .. 6.554 ms)
std dev              447.9 μs   (274.7 μs .. 583.2 μs)
variance introduced by outliers: 42% (moderately inflated)

benchmarking array unboxed: 1000/sequential
time                 4.598 ms   (4.592 ms .. 4.607 ms)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 4.588 ms   (4.583 ms .. 4.603 ms)
std dev              23.38 μs   (7.242 μs .. 47.44 μs)

benchmarking array unboxed: 1000/parallel
time                 9.857 ms   (9.252 ms .. 10.35 ms)
                     0.955 R²   (0.919 R² .. 0.981 R²)
mean                 7.430 ms   (6.802 ms .. 8.063 ms)
std dev              1.800 ms   (1.691 ms .. 1.969 ms)
variance introduced by outliers: 91% (severely inflated)

benchmarking vector boxed : 1000/sequential
time                 4.208 ms   (4.177 ms .. 4.259 ms)
                     0.999 R²   (0.998 R² .. 1.000 R²)
mean                 4.211 ms   (4.193 ms .. 4.268 ms)
std dev              83.33 μs   (23.68 μs .. 169.0 μs)

benchmarking vector boxed : 1000/parallel
time                 3.860 ms   (3.802 ms .. 3.915 ms)
                     0.998 R²   (0.998 R² .. 0.999 R²)
mean                 3.875 ms   (3.850 ms .. 3.909 ms)
std dev              102.9 μs   (86.24 μs .. 125.3 μs)
variance introduced by outliers: 11% (moderately inflated)

benchmarking vector unboxed : 1000/sequential
time                 3.624 ms   (3.618 ms .. 3.628 ms)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 3.622 ms   (3.620 ms .. 3.625 ms)
std dev              9.032 μs   (5.099 μs .. 14.83 μs)

benchmarking vector unboxed : 1000/parallel
time                 3.238 ms   (3.219 ms .. 3.262 ms)
                     0.999 R²   (0.999 R² .. 1.000 R²)
mean                 3.268 ms   (3.249 ms .. 3.284 ms)
std dev              56.28 μs   (48.49 μs .. 71.80 μs)

Benchmark matvec-diffstr: FINISH
masterdeg-0.1.0.0: benchmarks
Running 1 benchmarks...
Benchmark matvec-diffstr: RUNNING...
benchmarking list of lists : 5000/sequential
time                 145.5 ms   (127.1 ms .. 160.7 ms)
                     0.989 R²   (0.979 R² .. 0.999 R²)
mean                 137.4 ms   (132.5 ms .. 141.8 ms)
std dev              7.843 ms   (5.873 ms .. 9.676 ms)
variance introduced by outliers: 12% (moderately inflated)

benchmarking list of lists : 5000/parallel_par
time                 51.78 ms   (47.27 ms .. 58.28 ms)
                     0.967 R²   (0.937 R² .. 0.988 R²)
mean                 45.23 ms   (36.21 ms .. 49.11 ms)
std dev              10.25 ms   (4.611 ms .. 16.22 ms)
variance introduced by outliers: 77% (severely inflated)

benchmarking list of lists : 5000/parallel_strategy
time                 16.03 ms   (15.54 ms .. 16.27 ms)
                     0.996 R²   (0.988 R² .. 1.000 R²)
mean                 16.40 ms   (16.15 ms .. 17.39 ms)
std dev              1.173 ms   (76.43 μs .. 2.364 ms)
variance introduced by outliers: 32% (moderately inflated)

benchmarking array boxed: 5000/sequential
time                 241.9 ms   (236.4 ms .. 246.1 ms)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 238.4 ms   (232.5 ms .. 240.6 ms)
std dev              4.521 ms   (675.0 μs .. 6.010 ms)
variance introduced by outliers: 16% (moderately inflated)

benchmarking array boxed: 5000/parallel
time                 39.37 ms   (39.24 ms .. 39.50 ms)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 39.59 ms   (39.47 ms .. 39.81 ms)
std dev              350.1 μs   (187.0 μs .. 536.0 μs)

benchmarking array unboxed: 5000/sequential
time                 125.8 ms   (123.3 ms .. 128.9 ms)
                     0.999 R²   (0.998 R² .. 1.000 R²)
mean                 126.6 ms   (125.7 ms .. 127.4 ms)
std dev              1.264 ms   (766.1 μs .. 1.917 ms)
variance introduced by outliers: 11% (moderately inflated)

benchmarking array unboxed: 5000/parallel
time                 36.40 ms   (35.03 ms .. 37.96 ms)
                     0.993 R²   (0.985 R² .. 0.999 R²)
mean                 36.54 ms   (35.86 ms .. 37.73 ms)
std dev              2.066 ms   (964.1 μs .. 3.279 ms)
variance introduced by outliers: 18% (moderately inflated)

benchmarking vector boxed : 5000/sequential
time                 112.5 ms   (112.3 ms .. 112.9 ms)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 112.6 ms   (112.4 ms .. 113.0 ms)
std dev              444.8 μs   (118.2 μs .. 671.9 μs)
variance introduced by outliers: 11% (moderately inflated)

benchmarking vector boxed : 5000/parallel
time                 18.89 ms   (18.56 ms .. 19.11 ms)
                     0.998 R²   (0.995 R² .. 1.000 R²)
mean                 19.21 ms   (19.01 ms .. 19.92 ms)
std dev              833.9 μs   (141.9 μs .. 1.650 ms)
variance introduced by outliers: 13% (moderately inflated)

benchmarking vector unboxed : 5000/sequential
time                 92.40 ms   (92.38 ms .. 92.45 ms)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 92.40 ms   (92.38 ms .. 92.42 ms)
std dev              35.00 μs   (18.81 μs .. 53.77 μs)

benchmarking vector unboxed : 5000/parallel
time                 10.46 ms   (10.36 ms .. 10.55 ms)
                     0.999 R²   (0.999 R² .. 1.000 R²)
mean                 10.45 ms   (10.39 ms .. 10.59 ms)
std dev              233.6 μs   (105.9 μs .. 417.2 μs)

Benchmark matvec-diffstr: FINISH
