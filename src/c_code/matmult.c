#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
#include "utils.h"

void setup(double *a[], double *b[], double *c[], int n);
void mat_mul(double *a[], double *b[], double *c[], int n);
void mat_mul_par(double *a[], double *b[], double *c[], int n);
void validate_args(int argc, char *argv[]);
double reduce(double **c, int n);
void warmup(double *a[], double *b[], double *c[], int n);

int main(int argc, char *argv[])
{
  validate_args(argc, argv);

  int i;
  struct timeval t1, t2;
  double **a;
  double **b;
  double **c;

  int n = atoi(argv[1]);
  char *parOrSeq = argv[2];

  a = calloc(n, sizeof(double *));
  b = calloc(n, sizeof(double *));
  c = calloc(n, sizeof(double *));

  for (i = 0; i < n; ++i)
  {
    a[i] = calloc(n, sizeof(double));
    b[i] = calloc(n, sizeof(double));
    c[i] = calloc(n, sizeof(double));
  }

  setup(a, b, c, n);
  warmup(a, b, c, n);
  gettimeofday(&t1, NULL);

  if (strcmp(parOrSeq, "--seq") == 0)
  {
    mat_mul(a, b, c, n);
  }
  else if (strcmp(parOrSeq, "--par") == 0)
  {
    mat_mul_par(a, b, c, n);
  }
  else
  {
    printf("Unknown value : %s\n", parOrSeq);
    exit(-1);
  }

  gettimeofday(&t2, NULL);

#ifdef PROD
  output_time(&t1, &t2);
#else
  double exec_time = time_diff(&t1, &t2);
  double reduced_result = reduce(c, n);
  printf("reduced result: %lf\n", reduced_result);
  printf("%s: %lf\n", parOrSeq, exec_time);
#endif
}

void validate_args(int argc, char *argv[])
{
  char *message = "Invalid args. Accepted in order :\n N - size of matrix\n --par or --seq";
  if (argc < 3)
  {
    printf("%s", message);
    exit(-1);
  }
}

void setup(double *a[], double *b[], double *c[], int n)
{
  int i, j;
  for (i = 0; i < n; ++i)
  {
    for (j = 0; j < n; ++j)
    {
      a[i][j] = i / (j + 1);
      b[i][j] = j / (i + 1);
      c[i][j] = 0.0;
    }
  }
}

void warmup(double *a[], double *b[], double *c[], int n)
{
  int i, j;
  for (i = 0; i < 5; ++i)
  {
    mat_mul(a, b, c, n);

    for (j = 0; j < n; ++j)
    {
      memset(c[j], 0, n * sizeof(double));
    }
  }
}

void mat_mul(double *a[], double *b[], double *c[], int n)
{
  int i, j, k;

  for (i = 0; i < n; i++)
  {
    for (k = 0; k < n; k++)
    {
      for (j = 0; j < n; j++)
      {
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}

void mat_mul_par(double *a[], double *b[], double *c[], int n)
{
#pragma omp parallel default(none) shared(c, a, b, n)
  {
    int i, j, k;
#pragma omp for schedule(static) nowait
    for (i = 0; i < n; i++)
    {
      for (k = 0; k < n; k++)
      {
        for (j = 0; j < n; j++)
        {
          c[i][j] += a[i][k] * b[k][j];
        }
      }
    }
  }
}

double reduce(double **c, int n)
{
  int i, j;
  double reduced = 0.0;

  for (i = 0; i < n; ++i)
  {
    for (j = 0; j < n; ++j)
    {
      reduced += c[i][j];
    }
  }

  return reduced;
}