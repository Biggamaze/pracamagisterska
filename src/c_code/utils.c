#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils.h"

double time_diff(struct timeval *start, struct timeval *end)
{
    long seconds = end->tv_sec - start->tv_sec;
    long microseconds = ((seconds * 1e6) + end->tv_usec) - start->tv_usec;
    return microseconds / 1e6;
}

void output_time(struct timeval *start, struct timeval *end)
{
    printf("%lf", time_diff(start, end)*1000); // s -> ms
}
