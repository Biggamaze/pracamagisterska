#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include "utils.h"

void matvec(double **mat, double *vec, int N, double *res);
void matvec_par(double **mat, double *vec, int N, double *res);
void validate_args(int argc, char *argv[]);
void warmup(double **mat, double *vec, int N, double *res);

int main(int argc, char *argv[])
{
  int i, j;
  struct timeval t1, t2;

  validate_args(argc, argv);
  int N = atoi(argv[1]);
  char *parOrSeq = argv[2];


  double **mat = calloc(N, sizeof(double*));
  for (i = 0; i < N ; ++i)
  {
    mat[i] = calloc(N, sizeof(double));
  }
  double *vec = malloc(N * sizeof(double));
  double *res = calloc(N, sizeof(double));

  for (i = 0; i < N; ++i)
  {
    for (j = 0; j < N; ++j)
    {
      mat[i][j] = i / (j + 1);
    }
  }

  for (i = 0; i < N; ++i)
  {
    vec[i] = i / 0.99;
  }

  warmup(mat, vec, N, res);

  gettimeofday(&t1, NULL);

  if (strcmp(parOrSeq, "--seq") == 0)
  {
    matvec(mat, vec, N, res);
  }
  else if (strcmp(parOrSeq, "--par") == 0)
  {
    matvec_par(mat, vec, N, res);
  }
  else 
  {
    printf("Unknown argument: %s\n", parOrSeq);
    exit(-1);
  }

  gettimeofday(&t2, NULL);
  output_time(&t1, &t2);

  free(mat);
  free(vec);
  free(res);

  return 0;
}

void warmup(double **mat, double *vec, int N, double *res)
{
  int i;
  for (i = 0; i < 5; ++i)
  {
    matvec(mat, vec, N, res);
    memset(res, 0, N*sizeof(double));
  }
}

void matvec(double **mat, double *vec, int N, double *res)
{
  int i, j;

  for (i = 0; i < N; ++i)
  {
    for (j = 0; j < N; ++j)
    {
      res[i] += mat[i][j] * vec[j];
    }
  }
}

void matvec_par(double **mat, double *vec, int N, double *res)
{

#pragma omp parallel default(none) shared(mat, vec, res, N)
  {
    int i, j;

#pragma omp for nowait
    for (i = 0; i < N; ++i)
    {
      for (j = 0; j < N; ++j)
      {
        res[i] += mat[i][j] * vec[j];
      }
    }
  }
}

void validate_args(int argc, char *argv[])
{
  char *message = "Invalid args. Accepted in order :\n N - size of matrix\n --par or --seq\n";
  if (argc < 3)
  {
    printf("%s\n", message);
    exit(-1);
  }
}