#ifndef TIME_MES
#define TIME_MES

#include <sys/time.h>

double time_diff(struct timeval *start, struct timeval *end);
void output_time(struct timeval *start, struct timeval *end);

#endif