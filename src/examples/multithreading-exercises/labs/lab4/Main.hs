module Main where 

import Control.Concurrent
import Control.Concurrent.MVar
import Utility
import Control.Monad
import Control.Exception.Base

main = version1

mugs = 3 :: Int
taps = 2 :: Int

type Mugs = MVar Int
type Liters = Int
type Refill = MVar Int -> IO ()

-- wersja 1

version1 = do
  vMug <- newMVar mugs
  tap <- newMVar taps
  print $ "Taps available: " ++ (show taps)
  print $ "Mugs available: " ++ (show mugs)
  vars <- forM [5..7] $ \n -> do
    var <- forkThread (clientJob (refill tap) vMug n)
    return var
  wait vars

refillTime = 100000

clientJob :: Refill -> Mugs -> Liters -> IO ThreadId
clientJob refillF vMugs lit
  = let 
      drink curL
        | curL <= 0 = do
          id <- myThreadId
          print $ "Client " ++ show id ++ " drunk all. Leaving!"
          return id

        | otherwise = do
          id <- myThreadId
          print $ "Client " ++ show id ++ " attempting to take mug"
          var <- takeMVar vMugs
          if (var > 0) then do
            putMVar vMugs (var - 1)
            print $ "Client " ++ show id ++ " took mug .. " ++ show (var - 1) ++ " mugs available"
            print $ "Client " ++ show id ++ " drunk " ++ show (lit - curL + 1) ++ " liters - " ++ show (curL - 1)++ " liters to go!"
            print $ "Client " ++ show id ++ " put mug back! "
            forkIO $ refillF vMugs
            drink (curL - 1)
          else do
            print $ "Client " ++ show id ++ " waiting for mug :("
            threadDelay refillTime
            putMVar vMugs var
            drink curL
    in do
      id <- myThreadId
      print $ "Client " ++ show id ++ " entered pub! Want to drink " ++ (show lit) ++ " liters!"
      drink lit

refill :: MVar Int -> Mugs -> IO ()
refill tapMV mugsMV
  = do
      taps <- takeMVar tapMV
      if (taps <= 0) then do
        putMVar tapMV taps
        print $ "Not taps free for refill ... Waiting"
        threadDelay refillTime
      else do
        putMVar tapMV (taps - 1)
        print $ "Refilling mug ..."
        threadDelay refillTime
        modifyMVar_ tapMV (\n -> return (n+1))
        modifyMVar_ mugsMV (\n -> return (n+1))
        print $ "Refilled ..."

wait vars = do
  forM_ vars $ \v -> do
    val <- takeMVar v
    case val of
      (Right c) -> print $ "Client " ++ show c ++ " left"
      (Left e)  -> print $ "Finished with error " ++ show e