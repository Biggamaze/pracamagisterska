module Main where

import Control.Parallel.Strategies
import Data.Ratio
import Data.Number.CReal

main = do
  print $ (integrateTPar (\x -> x^4 + x^3 + x^2 + x/13 + 1) (0.0,10000.0) 0.01 :: Float)

integrateT :: (RealFrac a, Enum a, NFData a) => (a -> a) -> (a,a) -> a -> a 
integrateT f (ini, fin) dx 
  = let lst = (map f [ini,ini+dx..fin]) 
    in sum lst * dx - 0.5 * (f ini + f fin) * dx

-- przez zrównoleglenie tracimy na dokładności w ten sposób!
integrateTPar :: (RealFrac a, Enum a, NFData a) => (a -> a) -> (a,a) -> a -> a 
integrateTPar f (l,r) dx 
  = let chunks = [ integrateT f (l + i*wChunk, l + (i + 1)*wChunk) dx 
                  | i <- [0..nChunks-1] ] `using` parList rdeepseq
    in sum chunks
      where nChunks = 100
            wChunk = (r-l)/nChunks 

evalListChunks :: Int -> Strategy [a] -> Strategy [a]
evalListChunks _ _ [] = return []
evalListChunks chunkSize strat l@(x:xs) 
  = let (ch, rs) = splitAt chunkSize l
    in do
      ch' <- strat ch
      rs' <- evalListChunks chunkSize strat rs
      return (ch' ++ rs') 

parListChunk' :: (NFData a) => Int -> Strategy [a]
parListChunk' size = evalListChunks size (rparWith rdeepseq) 
