module Main where 

import Control.Concurrent
import Control.Monad
import Control.DeepSeq

noThreads = 100000 :: Int
threadIds = [0..noThreads]

main = f3

f1 = do
  ids <- forM threadIds $ \id ->
    forkThread $ threadFunc id
  mapM_ takeMVar ids

threadFunc :: Int -> IO ()
threadFunc val = do
  id <- myThreadId
  print $ "My passed value: " ++ show val ++ " My threadId: " ++ show id

forkThread :: IO () -> IO (MVar ())
forkThread io = do
  handle <- newEmptyMVar
  forkFinally io (\_ -> putMVar handle ())
  return handle

----

f2 = do
  let some = Some 1 "Name" 1.1
  var <- forkThread $ threadFunc2 some
  takeMVar var

data SomeData = Some
  { num :: Int
  , name :: String
  , age :: Double
  } deriving (Show)

threadFunc2 :: SomeData -> IO ()
threadFunc2 a@(Some _ _ _) = do
  print $ show (num a) ++ name a ++ show (age a)

---
f3 = forM_ [0..100] $ \i -> f3impl

f3impl = do
  let some = Some 1 "init" 0.1
  var <- newMVar some
  mut1 <- forkThread $ threadFunc3 var
  mut2 <- forkThread $ threadFunc3 var
  takeMVar mut1
  takeMVar mut2
  val <- takeMVar var
  print $ val

threadFunc3 :: MVar SomeData -> IO ()
threadFunc3 var = do
  id <- myThreadId
  modifyMVar_ var $ \(Some n s d) ->
    let some = Some (n+1) (s ++ "_" ++ show id ++ "_") (d+0.1)
    in return some -- tu coś jest nie tak - jak wymusić na wątku obliczenia?