module Threading.Basic where

import Control.Concurrent

-- brak wymuszania ewaluacji
first :: IO ()
first = do
  id1 <- forkIO $ print "JEDEN"
  id2 <- forkIO $ print "DWA"
  id3 <- forkIO $ print "TRZY"
  print "KONIEC"

-- wymuszanie (?) ewaluacji
second :: IO ()
second = do
  id1 <- forkIO $ print "JEDEN"
  id2 <- forkIO $ print "DWA"
  id3 <- forkIO $ print "TRZY"
  print $ show [id1, id2, id3]

third :: IO ()
third = do 
  id <- forkIO $ takeALongTime
  print "Koniec"

takeALongTime :: IO ()
takeALongTime = threadDelay 50000 >> print "End!"

