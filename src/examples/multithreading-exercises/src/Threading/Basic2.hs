module Threading.Basic2 where 

import Data.List
import Text.Read
import Control.Applicative
import Control.Monad

type DFunc = (Double -> Double -> Double)

data Expression = Operator Ingredient Expression Expression
                | Operand Ingredient

data Ingredient = Op DFunc 
                | Numb Double 

calculate :: [String] -> Maybe Double
calculate inpt = undefined . mapInput $ inpt

-- TODO: Zapytać na SO czemu z foldM się nie kompiluje
toExpression :: [Ingredient] -> Maybe Expression
toExpression (i1@(Numb i):is) = 
  let result = sequence . foldl bTree initState $ is
      initState = [Just $ Operand i1]  
      bTree exprs ing = case ing of 
        (Op func) -> undefined
        (Numb n)  -> undefined 
  in undefined

mapInput :: [String] -> Maybe [Ingredient]
mapInput input = sequence . map toIngredient $ input
  where toIngredient str = case lookup str operators of
          Nothing   -> Numb <$> (readMaybe str)
          (Just op) -> Just (Op op)

operators = [("+", (+))
            ,("-", (-))
            ,("*", (*))
            ,("/", (/))] 

-------------------------------

newtype Stack a = Stack [a]

push :: Stack a -> a -> Stack a
push (Stack xs) y = Stack (y:xs)

pop :: Stack a -> (Maybe a, [a])
pop (Stack []) = (Nothing, [])
pop (Stack (x:xs)) = (Just x,xs)