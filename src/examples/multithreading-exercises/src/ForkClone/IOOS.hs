module ForkClone.IOOS where

import Control.Monad
import Control.Concurrent
import Control.Concurrent.MVar

-- lab 2

forkClone :: IO ()
forkClone = do
  var <- newMVar (0::Int)
  let tasks = replicate 100 task1
  mapM_ ($ var) tasks
  val <- takeMVar var
  print $ "Final value: " ++ show val

task1 :: Num a => MVar a -> IO ()
task1 var = do
  val <- takeMVar var
  putMVar var (val + 1) 

-- lab3

threadsNo = 1000
threadsIds = take threadsNo [(1,1)]