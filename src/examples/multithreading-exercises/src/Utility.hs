module Utility where

import Control.Concurrent
import Control.Concurrent.MVar

forkThread io = do
  var <- newEmptyMVar
  forkFinally io (\v -> putMVar var v)
  return var