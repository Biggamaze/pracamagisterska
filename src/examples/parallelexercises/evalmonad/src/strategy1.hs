module Main where 

import Control.Parallel.Strategies
import Control.DeepSeq

main = print $ show comPair

-- type Strategy a = a -> Eval a

parPair' :: Strategy (a,b)
parPair' = \(a,b) -> do
  a' <- rpar a
  b' <- rpar b
  return (a', b')

comPair = (sum [0..100000], sum [0..100000]) `using` parPair'

evalPair :: Strategy a -> Strategy b -> (a,b) -> Eval (a,b)
evalPair sa sb = \(a,b) -> do
  a' <- sa a
  b' <- sb b
  return (a', b')

parPair'' :: (a, b) -> Eval (a,b) 
parPair'' (a,b) = evalPair rpar rpar (a,b)


-- chyba chodzi z tym rseq o wymuszenie poczekania na efekty force x
rdeepseq' :: NFData a => Strategy a
rdeepseq' x = rseq (force x)

-- ciężko to zapakowac w rpar teraz więć używa się funkcji rparWith
parPair''' :: Strategy a -> Strategy b -> Strategy (a,b)
parPair''' sa sb = evalPair (rparWith sa) (rparWith sb)

-- moje zabawki

data Dif = Hard
           { hlist :: [Int]
           , hcomp :: [Int] -> Int
           , hstr  :: String } 

instance Show Dif where
  show (Hard l _ s) = "Hard " ++ show l ++ " | " ++ show s

evalDif :: Strategy [Int] -> Strategy ([Int] -> Int) -> Strategy String -> Dif -> Eval Dif
evalDif sa sb sc a@(Hard l c s) = do
  l' <- sa l
  c' <- sb c
  s'  <- sc s
  return (Hard l' c' s')

parDif :: Strategy [Int] -> Strategy ([Int] -> Int) -> Strategy String -> Strategy Dif
parDif sa sb sc = evalDif (rparWith sa) (rparWith sb) (rparWith sc) 