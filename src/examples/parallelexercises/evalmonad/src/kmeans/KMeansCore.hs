{-# LANGUAGE BangPatterns #-}

module KMeansCore where 

data Point = Point !Double !Double
             deriving (Show, Eq)

zeroPoint :: Point
zeroPoint = Point 0 0 

sqDistance :: Point -> Point -> Double
sqDistance (Point x1 y1) (Point x2 y2) = ((x2-x1)^2) + ((y2-y1)^2)

data Cluster 
  = Cluster { clId   :: Int
            , clCent :: Point } deriving (Show, Eq)

data PointSum = PointSum !Int !Double !Double