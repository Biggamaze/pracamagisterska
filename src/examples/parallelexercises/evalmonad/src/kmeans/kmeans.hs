module Main where

import KMeansCore
import qualified Data.Vector as V 
import qualified Data.Vector.Mutable as VM 
import Data.List
import Data.Function

main = undefined

addPointToSum :: PointSum -> Point -> PointSum
addPointToSum (PointSum count xs ys) (Point x y) 
  = PointSum (count + 1) (xs + x) (ys + y)

pointSumToCluster :: Int -> PointSum -> Cluster
pointSumToCluster i (PointSum count xs ys) =
  Cluster { clId = i
          , clCent = Point (xs/fromIntegral count) (ys/fromIntegral count)
          }

assign :: Int -> [Cluster] -> [Point] -> V.Vector PointSum
assign nclusters clusters points = V.create $ do
  vec <- VM.replicate nclusters (PointSum 0 0 0)
  let 
    addpoint p = do
      let c = nearest p; cid = clId c
      ps <- VM.read vec cid
      VM.write vec cid $! addPointToSum ps p
  mapM_ addpoint points
  return vec
  where 
    nearest p = fst $ minimumBy (compare `on` snd)
                        [(c,sqDistance (clCent c) p) | c <- clusters]

makeNewCluster :: V.Vector PointSum -> [Cluster]
makeNewCluster vec = 
  [ pointSumToCluster i ps 
  | (i, ps@(PointSum count _ _)) <- zip [0..] (V.toList vec) 
  , count > 0
  ]

step :: Int -> [Cluster] -> [Point] -> [Cluster]
step nclusters clusters points =
  makeNewCluster (assign nclusters clusters points)

tooMany = 80

kmeans_seq :: Int -> [Point] -> [Cluster] -> IO [Cluster]
kmeans_seq nclusters points clusters = 
  let 
    loop :: Int -> [Cluster] -> IO [Cluster]
    loop n clusters 
      | n > tooMany = do 
        putStrLn "Giving up."
        return clusters
    loop n clusters = do
      print $ "Iteration " ++ show n ++ "\n"
      putStr (unlines (map show clusters))
      let clusters' = step nclusters clusters points
      if clusters' == clusters
        then return clusters
        else loop (n+1) clusters'
  in loop 0 clusters