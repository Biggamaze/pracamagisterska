module Main where

import Strategies
import System.Random
import Control.Parallel.Strategies
import Control.DeepSeq

main = do
  let xs = (map fib (genInput 10000)) `using` listStrat 50
  print $ length (filter  (>0) xs)

fib :: Int -> Int
fib 1 = 1
fib 2 = 1
fib n = fib (n-1) + fib (n-2)

genInput :: Int -> [Int]
genInput n = take n $ cycle [25]

-- STRATEGIES

listStrat :: NFData a => Int -> Strategy [a]
listStrat _ [] = return []
listStrat size a@(x:xs) = 
  let 
    (chunk, rest) = splitAt size a
  in do 
    chunk' <- rpar (force chunk)
    rest'  <- listStrat size rest
    return (chunk' ++ rest')
