module Main where

import Control.Parallel.Strategies
import Control.Concurrent

main = do
  let (a,b) = run
  print $ "Finished: " ++ show a ++ show b

run = runEval $ do
  a <- rseq expensive
  b <- rseq expensive
  return (a,b) 

expensive = sum [1..10000000]