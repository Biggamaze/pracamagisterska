module Main where 

import Sudoku
import Control.Exception
import System.Environment
import Data.Maybe
import Control.Parallel.Strategies
import Control.DeepSeq

main :: IO ()
main = do
  [f] <- getArgs
  file <- readFile f

  let puzzles = lines file
      solutions = runEval $ parMap' solve puzzles

  print (length (filter isJust solutions))

parMap' :: (a -> b) -> [a] -> Eval [b]
parMap' f [] = return []
parMap' f (x:xs) = do
  y <- rpar $ f x
  ys <- parMap' f xs
  return (y:ys)