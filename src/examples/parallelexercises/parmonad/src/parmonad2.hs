module Main where

import Control.Monad.Par

main :: IO ()
main = do 
  let res = runPar $ parMapM' fib [1..38]
  let res2 = runPar $ parMapM' fib [39..40]
  print $ sum res + sum res2

spawn' :: NFData a => Par a -> Par (IVar a)
spawn' p = do
  i <- new 
  fork (do x <- p; put i x)
  return i

parMapM' :: NFData b => (a -> b) -> [a] -> Par [b]
parMapM' f as = do
  ibs <- mapM (spawn' . return . f) as
  mapM get ibs

fib :: Int -> Integer
fib n
  | n <= 2    = 1 
  | otherwise = fib (n-1) + fib (n-2)
