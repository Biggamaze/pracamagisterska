module Main where

import Control.Monad.Par

main :: IO ()
main = do
  print $ 0 < parFnM
  --print $ 0 < (fib 40 + fib 39)

parFnM = runPar $ do
  b <- new
  c <- new
  fork (put b (fib 40))
  fork (put c (fib 39))
  v1 <- get b
  v2 <- get c
  return (v1 + v2)

fib :: Int -> Integer
fib n
  | n <= 2    = 1 
  | otherwise = fib (n-1) + fib (n-2)

calc f n = sum $ map f [1..n]