module Main where

import           Criterion
import           Criterion.Main                 (defaultMain)
import           System.Random
import           Data.List
import           Control.DeepSeq

import Algorithm.Paralell.Matmult

mat :: Int -> [[Double]]
mat size = replicate size . take size $ [1..]

main :: IO ()
main = do
  defaultMain 
    [ sequentialVsParallel 1
    , sequentialVsParallel 10
    , sequentialVsParallel 100
    , sequentialVsParallel 500
    ]

sequentialVsParallel n 
  = bgroup ("size_" ++ show n ++ "x" ++ show n) 
    [ bench "sequential" $ nf (matmultList $ mat n) (mat n)
    , bench "parallel" $ nf (matmultListPar $ mat n) (mat n)
    , bench "strategy" $ nf (matmultListStrategy $ mat n) (mat n)
    ]
