module Main where

import qualified Algorithm.Paralell.Matmult.Array         as AM
import qualified Algorithm.Paralell.Matmult.ArrayUnboxed  as AMU
import           Algorithm.Paralell.Matmult.List
import qualified Algorithm.Paralell.Matmult.Vector        as MV
import qualified Algorithm.Paralell.Matmult.VectorUnboxed as MVU
import           Control.Concurrent                       (getNumCapabilities, setNumCapabilities)
import           Control.DeepSeq
import           Criterion
import           Criterion.Main                           (defaultMain)
import           System.Environment                       (getEnv)
import           Text.Read
import qualified Utils.DataGeneration                     as DG
import           Utils.Helpers

main :: IO ()
main = do
  args <- getEnv "MASTERDEG_HASKELL_DIMENSIONS"
  capabilities <- getNumCapabilities

  let benchmarks = case parseDimensions args of
                    Nothing  -> error "Unable to parse environment variable MASTERDEG_HASKELL_DIMENSIONS"
                    Just a   -> createBenchmarks a capabilities

  defaultMain benchmarks


createBenchmarks n capabilities
  = concat . fmap creator $ n
    where creator size = [ array size
                         , arrayUnboxed size
                         , vector size
                         , vectorUnboxed size
                         , list size capabilities ]

list n capabilities
  = env (return (DG.matAsListOfLists n))
    $ \ ~(mat) -> bgroup ("list of lists : " ++ show n)
    [ bench "sequential" $ nf (matmultList mat) mat
    , bench "parallel_par" $ nf (matmultListPar mat) mat
    , bench "parallel_strategy" $ nf (matmultListStrategy mat) mat
    , bench "parallel_par_chunks" $ nf (matmultListParChunks (n `quot` capabilities) mat ) mat
    ]

array n
  = env (return (DG.matArray n))
    $ \ ~(mat) -> bgroup ("array: " ++ show n)
    [ bench "sequential" $ nf (AM.matmult mat) mat
    , bench "parallel with strategy" $ nf (AM.matmultStrategy mat) mat
    ]

arrayUnboxed n
  = env (return (DG.matUArray n))
    $ \ ~(mat) -> bgroup ("array unboxed: " ++ show n)
    [ bench "sequential" $ nf (AMU.matmult mat) mat
    , bench "parallel with strategy" $ nf (AMU.matmultPar mat) mat
    ]

vector n
  = env (return (DG.matAsVecOfVecBoxed n))
    $ \ ~(mat) -> bgroup ("vector: " ++ show n)
    [ bench "sequential" $ nf (MV.matmult mat) mat
    , bench "parallel with strategy" $ nf (MV.matmultPar mat) mat
    ]

vectorUnboxed n
  = env (return (DG.matAsVecOfVecUnboxed n))
    $ \ ~(mat) -> bgroup ("vector unboxed: " ++ show n)
    [ bench "sequential" $ nf (MVU.matmult mat) mat
    , bench "parallel with strategy" $ nf (MVU.matmultPar mat) mat
    ]
