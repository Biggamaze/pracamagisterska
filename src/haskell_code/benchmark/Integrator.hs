module Main where

import           Criterion
import           Criterion.Main                 (defaultMain)
import           System.Random

import           Algorithm.Paralell.Integration

main :: IO ()
main = do
  let f x = integrateT (\x -> x ^ 4 + x ^ 3 + x ^ 2 + x / 13 + 1) ((0, x) :: (Double, Double))
  let f_par x = integrateT_par (\x -> x ^ 4 + x ^ 3 + x ^ 2 + x / 13 + 1) ((0, x) :: (Double, Double))
  let f_strat x = integrateT_strategy (\x -> x ^ 4 + x ^ 3 + x ^ 2 + x / 13 + 1) ((0, x) :: (Double, Double))
  let dx = 0.01
  defaultMain
    [ bgroup
        "integration seq "
        [ bench "seq 10" $ whnf (f 10) dx
        , bench "seq 100" $ whnf (f 100) dx
        , bench "seq 1000" $ whnf (f 1000) dx
        , bench "seq 10000" $ whnf (f 10000) dx
        ]
    , bgroup
        "integration par "
        [ bench "par 10" $ whnf (f_par 10) dx
        , bench "par 100" $ whnf (f_par 100) dx
        , bench "par 1000" $ whnf (f_par 1000) dx
        , bench "par 10000" $ whnf (f_par 10000) dx
        ]
    , bgroup
        "integration strategy "
        [ bench "startegy 10" $ whnf (f_strat 10) dx
        , bench "startegy 100" $ whnf (f_strat 100) dx
        , bench "startegy 1000" $ whnf (f_strat 1000) dx
        , bench "startegy 10000" $ whnf (f_strat 10000) dx
        ]
    ]
