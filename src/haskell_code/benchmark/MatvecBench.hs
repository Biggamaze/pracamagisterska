module Main where

import           Criterion
import           Criterion.Main                 (defaultMain)
import           System.Random
import           Data.List
import qualified Data.Vector                    as V
import           Control.DeepSeq

import Algorithm.Paralell.Matvec.List
import Algorithm.Paralell.Matvec.Vector

matVector n = V.enumFromN 0 (n*n) :: V.Vector Double
vecVector n = V.enumFromN 0 n :: V.Vector Double

matAsOneList n = take (n*n) . iterate (+1) $ 0

matAsListOfLists n = replicate n row
  where row = vecList n

vecList :: Int -> [Double]
vecList n = take n . iterate (+1) $ 0

main :: IO ()
main = do
  defaultMain 
    [ sequentialImplComparison 1
    , sequentialImplComparison 10
    , sequentialImplComparison 100
    , sequentialImplComparison 1000
    -- , sequentialImplComparison 10000
    , parallelImplComparison 1
    , parallelImplComparison 10
    , parallelImplComparison 100
    , parallelImplComparison 1000
    -- , parallelImplComparison 10000
    ]

sequentialImplComparison n 
  = bgroup ( "sequential_size" ++ show n) [
      bench "vector" $ nf (matvecAsOneVectorBoxed (matVector n)) (vecVector n)
    , bench "matAsListOfLists" $ nf (matvecList (matAsListOfLists n)) (vecList n)
    ]

parallelImplComparison n 
  = bgroup ( "parallel_size" ++ show n) [
      bench "vector" $ nf (matvecParAsOneVectorBoxed (matVector n)) (vecVector n)
    , bench "matAsListOfLists" $ nf (matvecListPar (matAsListOfLists n)) (vecList n)
    ]