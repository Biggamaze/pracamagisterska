module Main where

import qualified Algorithm.Paralell.Matvec.Vector         as MVU
import           Control.Concurrent                       (getNumCapabilities, setNumCapabilities)
import           Criterion
import           Criterion.Main                           (defaultMain)
import           Control.Monad
import qualified Utils.DataGeneration                     as DG
import           System.Environment                       (getEnv)
import           Text.Read

main :: IO ()
main = do
  dimension <- getEnv "THREADS_BENCH_DIMENSION"
  capabilities <- getNumCapabilities
  defaultMain (createBenchmarks capabilities (read dimension :: Int))

createBenchmarks :: Int -> Int -> [Benchmark]
createBenchmarks capabilities dimension = sequential dimension : fmap (parallel dimension) [2..capabilities]

sequential dimension = env (return (DG.matAsVecOfVecUnboxed dimension, DG.vecAsVectorUnboxed dimension))
                       $ \ ~(mat, vec) -> bench ("matvec sequential/ dimension: " ++ show dimension) (nf (MVU.matvecAsVecOfVecUnboxed mat) vec)

parallel dimension cores = env (do setNumCapabilities cores
                                   return (DG.matAsVecOfVecUnboxed dimension, DG.vecAsVectorUnboxed dimension))
                           $ \ ~(mat, vec) -> bench ("matvec par/ threads: " ++ show cores ++ "/dimension: " ++ show dimension) (nf (MVU.matvecParAsVecOfVecUnboxed mat) vec)