module Main where

import qualified Algorithm.Paralell.Matvec.Array        as A
import qualified Algorithm.Paralell.Matvec.ArrayUnboxed as AU
import           Algorithm.Paralell.Matvec.List
import           Algorithm.Paralell.Matvec.Vector
import           Control.DeepSeq
import           Criterion
import           Criterion.Main                         (defaultMain)
import           System.Environment                     (getEnv)
import           Text.Read
import qualified Utils.DataGeneration                   as DG
import           Utils.Helpers

main :: IO ()
main = do
  args <- getEnv "MASTERDEG_HASKELL_DIMENSIONS"

  let benchmarks = case parseDimensions args of
                      Nothing  -> error "Unable to parse environment variable MASTERDEG_HASKELL_DIMENSIONS"
                      Just a        -> createBenchmarks a

  defaultMain benchmarks


createBenchmarks = concat . fmap creator
  where creator n = [ list n
                    , arrayBoxed n
                    , arrayUnboxed n
                    , vectorBoxed n
                    , vectorUnboxed n
                    ]

vectorBoxed n
  = env (return (DG.matAsVecOfVecBoxed n, DG.vecAsVectorBoxed n))
    $ \ ~(mat, vec) -> bgroup ("vector boxed : " ++ show n)
    [ bench "sequential" $ nf (matvecAsVecOfVecBoxed mat) vec
    , bench "parallel" $ nf (matvecParAsVecOfVecBoxed mat) vec
    ]

vectorUnboxed n
  = env (return (DG.matAsVecOfVecUnboxed n, DG.vecAsVectorUnboxed n))
    $ \ ~(mat, vec) -> bgroup ("vector unboxed : " ++ show n)
    [ bench "sequential" $ nf (matvecAsVecOfVecUnboxed mat) vec
    , bench "parallel" $ nf (matvecParAsVecOfVecUnboxed mat) vec
    ]

arrayBoxed n
  = env (return (DG.matArray n, DG.vecArray n))
    $ \ ~(mat, vec) -> bgroup ("array boxed: " ++ show n)
    [ bench "sequential" $ nf (A.matvec mat) vec
    , bench "parallel" $ nf (A.matvecPar mat) vec
    ]

arrayUnboxed n
  = env (return (DG.matUArray n, DG.vecUArray n))
    $ \ ~(mat, vec) -> bgroup ("array unboxed: " ++ show n)
    [ bench "sequential" $ nf (AU.matvec mat) vec
    , bench "parallel" $ nf (AU.matvecPar mat) vec
    ]

list n
  = env (return (DG.matAsListOfLists n, DG.vecAsList n))
    $ \ ~(mat, vec) -> bgroup ("list of lists : " ++ show n)
    [ bench "sequential" $ nf (matvecList mat) vec
    , bench "parallel_par" $ nf (matvecListPar mat) vec
    , bench "parallel_strategy" $ nf (matvecListStrategy mat) vec
    ]

