{-# LANGUAGE OverloadedStrings #-}

module WebSocketServer where

import           Control.Concurrent

import           Algorithm.Concurrent.PubSTM
import           Control.Exception           (finally)
import           Data.Text
import           Data.Text.IO
import qualified Network.WebSockets          as WS

webSocketServer :: Int -> IO ()
webSocketServer port = do
  state <- newMVar True
  WS.runServer "0.0.0.0" port $ application state

application :: MVar Bool -> WS.ServerApp
application state pending = do
  isFree <- tryTakeMVar state
  conn <- WS.acceptRequest pending
  WS.forkPingThread conn 30
  case isFree of
    Just True -> handleSuccessfulConnection state conn
    Nothing   -> handleFailedConnection state conn

handleSuccessfulConnection :: MVar Bool -> WS.Connection -> IO ()
handleSuccessfulConnection state conn =
  flip finally disconnect $ do
    WS.sendTextData conn ("Connection succesfull" :: Text)
    openPub (WS.sendTextData conn) 5 3 -- 5 clients, 3 mugs
    putMVar state True
  where
    disconnect = putMVar state True

handleFailedConnection :: MVar Bool -> WS.Connection -> IO ()
handleFailedConnection state conn = WS.sendTextData conn ("Another client already connected" :: Text)
