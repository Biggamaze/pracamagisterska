{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Server where

import           Web.Spock
import           Web.Spock.Config

import           Control.Monad.Trans
import           Controllers.EndpointActions (act)
import           Data.Aeson                  hiding (json)
import qualified Data.Map.Strict             as M
import           Data.Monoid                 ((<>))
import           Data.Text                   (Text, pack)
import           GHC.Generics

data ServiceResponse = ServiceResponse
  { message       :: Maybe [Text]
  , executionTime :: Maybe Double
  } deriving (Generic, Show)

instance ToJSON ServiceResponse

instance FromJSON ServiceResponse

type Api = SpockM () () () ()

type ApiAction a = SpockAction () () () a

server :: Int -> IO ()
server port = do
  spockCfg <- defaultSpockCfg () PCNoDatabase ()
  runSpock port (spock spockCfg app)

app :: Api
app = do
  get "testconnection" $ json $ ServiceResponse {message = Just ["Success"], executionTime = Nothing}
  get "integrate" $ do
    results <- lookupAction act "integrate" "null"
    json $ createResponse results
  get ("matvec" <//> var) $ \s -> do
    result <- lookupAction act "matvec" s
    json $ createResponse result
  get ("fib" <//> var) $ \n -> do
    result <- lookupAction act "fib" n
    json $ createResponse result
  get ("matmult" <//> var) $ \n -> do
    result <- lookupAction act "matmult" n
    json $ createResponse result
  get ("matmultList" <//> var) $ \n -> do
    result <- lookupAction act "matmultList" n
    json $ createResponse result

lookupAction dict name arg = liftIO $ (act M.! name) [arg] :: ApiAction [String]

createResponse message = ServiceResponse {message = Just (fmap pack message), executionTime = Nothing}
