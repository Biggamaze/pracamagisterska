module Main where

import           Control.Concurrent
import           Control.Parallel
import           System.Environment (getArgs)

import           Data.Maybe         (fromMaybe)
import           Server             (server)
import           Text.Read
import           WebSocketServer

defaultPort = 9080 :: Int

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["--no-server"] -> runLocal
    [] -> do
      putStrLn $ "No port specified defaulting to " ++ show defaultPort
      runServers defaultPort
    [a] -> do
      let port = fromMaybe defaultPort (readMaybe a)
      runServers port
    _ -> error "Too many arguments. Provide only a port number."

runServers :: Int -> IO ()
runServers port = do
  putStrLn $ "WebSocket server listens on port " ++ show (port + 1)
  forkOS $ webSocketServer (port + 1)
  server port

runLocal :: IO ()
runLocal = undefined
