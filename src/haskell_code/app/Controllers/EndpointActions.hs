module Controllers.EndpointActions where

import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import Data.List
import Control.DeepSeq
import System.Clock
import Algorithm.Paralell.Fibonacci
import Algorithm.Paralell.Matmult.List
import Algorithm.Paralell.Integration
import Algorithm.Concurrent.Pub
import Algorithm.Paralell.Matvec hiding (matvec)
import Control.Monad
import Data.Maybe
import Text.Read
import Control.Monad.Writer
import Control.Monad.Trans
import Data.Text (Text, pack)

availableOperations :: String
availableOperations = intercalate ", " . M.keys $ actions

act :: M.Map String ([String] -> IO [String])
act = M.fromList [
    ("integrate", execWriterT . mainIntegration1)
  , ("matvec", execWriterT . matvec)
  , ("fib", execWriterT . fibonacci)
  , ("matmult", execWriterT . matmultBenchmark)
  , ("matmultList", execWriterT . matmultBenchmarkList)
  ]

actions :: M.Map String ([String] -> IO ())
actions =  M.fromList [("pub", pubSimulation)]

mainIntegration1 :: [String] -> WriterT [String] IO ()
mainIntegration1 _ = do
  let f x = x ^ 4 + x ^ 3 + x ^ 2 + x / 13 + 1
  let domain = (0, 10000.0) :: (Float, Float)
  let dx = 1.0e-2
  let res_seq = integrateT f domain dx
  let res_strategy = integrateT_strategy f domain dx
  let res_par = integrateT_par f domain dx

  t1 <- liftIO . evalFuncWithTime $ res_seq
  t2 <- liftIO . evalFuncWithTime $ res_strategy
  t3 <- liftIO . evalFuncWithTime $ res_par

  tell ["Sequential: " ++ show t1]
  tell ["Strategy: " ++ show t2]
  tell ["Par combinator: " ++ show t3]
  tell ["All: " ++ show (res_strategy + res_seq + res_par)]

matvec :: [String] -> WriterT [String] IO ()
matvec args =
  case args of
    [] -> tell ["Empty args!"]
    [num] ->
      case (readMaybe num :: Maybe Int) of
        Just n ->
          if n > 9000
            then tell ["Too large n"]
            else (do sequential <- liftIO $ sequentialMatVec n
                     parallel <- liftIO $ parallelMatVec n
                     tell ["(arg evaluation time, calculation time)"]
                     tell [ "Sequential: " ++ show sequential]
                     tell [ "Parallel: " ++ show parallel])
        Nothing -> tell ["Arg not a number"]
    _ -> tell ["Too many args!"]

fibonacci :: [String] -> WriterT [String] IO ()
fibonacci args = do
  let elemNum = parse args
  t1 <- liftIO $ getTime Monotonic
  let f1 = fib elemNum
  sequential <- liftIO $ f1 `deepseq` getTime Monotonic
  t2 <- liftIO $ getTime Monotonic
  let f2 = fibPar elemNum
  parallel <- liftIO $ f2 `deepseq` getTime Monotonic
  t3 <- liftIO $ getTime Monotonic
  let f3 = fibParThreshold elemNum 10
  parThres <- liftIO $ f3 `deepseq` getTime Monotonic
  let seqTime = (toDouble sequential - toDouble t1) / (10 ^ 9)
  let parTime = (toDouble parallel - toDouble t2) / (10 ^ 9)
  let parTimeThres = (toDouble parThres - toDouble t3) / (10 ^ 9)
  tell ["Sequential: " ++ show seqTime ++ " [s] = " ++ show f1]
  tell ["Parallel: " ++ show parTime ++ " [s] = " ++ show f2]
  tell ["Parallel with threshold " ++ show 10 ++ ": " ++ show parTimeThres ++ " [s] = " ++ show f3]

matmultBenchmark :: [String] -> WriterT [String] IO ()
matmultBenchmark args =
  case args of
    [] -> tell ["Empty argument list ofr matmult"]
    (a:_) ->
      case (readMaybe a :: Maybe Double) of
        Nothing -> tell ["Unable to parse argument: " ++ a]
        Just size -> do
          let row = V.enumFromTo 0 size
          let col = V.enumFromTo 0 size
          let a = V.replicate (round size) row
          let b = V.replicate (round size) col
          time <- liftIO $ evalFuncWithTime (matmultPar a b :: Mat Double)
          tell [show time]

matmultBenchmarkList :: [String] -> WriterT [String] IO ()
matmultBenchmarkList args =
  case args of
    [] -> tell ["Empty argument list of matmult"]
    (a:_) ->
      case (readMaybe a :: Maybe Double) of
        Nothing -> tell ["Unable to parse argument: " ++ a]
        Just size -> do
          let row = [0 .. size]
          let col = [0 .. size]
          let a = replicate (round size) row
          let b = replicate (round size) col
          time <- liftIO $ evalFuncWithTime (matmultList a b)
          tell [show time]

{-
  Helpers
-}

evalFuncWithTime :: (NFData a) => a -> IO Double
evalFuncWithTime f = do
  t1 <- getTime Monotonic
  sequential <- f `deepseq` getTime Monotonic
  let time = (toDouble sequential - toDouble t1) / (10 ^ 9)
  return time
parse :: [String] -> Int
parse [] = error "Empty args!"
parse [a] = fromMaybe (error "") (readMaybe a)
parse _ = error "Too many args"

toDouble :: TimeSpec -> Double
toDouble ts = fromIntegral (toNanoSecs ts)