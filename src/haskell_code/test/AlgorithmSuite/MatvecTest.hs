module AlgorithmSuite.MatvecTest where

import           Control.Monad
import           Test.HUnit
import           Test.HUnit.Base
import qualified Data.Vector as V
import qualified Data.Array as DA
import qualified Data.Array.Unboxed as DAU
import           Algorithm.Paralell.Matvec.List
import           Algorithm.Paralell.Matvec.Vector
import qualified Algorithm.Paralell.Matvec.Array as MA
import qualified Algorithm.Paralell.Matvec.ArrayUnboxed as MAU

matAsListOfListCases :: [([Double], [[Double]], [Double])]
matAsListOfListCases =
  [ ([2, 2], [[1, 1], [1, 1]], [1, 1])
  , ([0], [[0]], [0])
  , ([1], [[1]], [1])
  , ([0], [[0]], [0])
  , ([5, 5], [[1, 2], [1, 2]], [1, 2])
  ]

matAsAListCases = map flatten matAsListOfListCases
  where
    flatten (exp, mat, vec) = (exp, concat mat, vec)

vectorAsOneVectorBoxedTestCases = map toVector matAsAListCases
  where
    toVector (exp, mat, vec) = (V.fromList exp, V.fromList mat, V.fromList vec)

arrayBoxedTestCases = map f matAsListOfListCases
  where
    f (exp, mat, vec) = (toArrayVec exp, toArrayMat mat, toArrayVec vec)
    toArrayVec a = let bounds = (0, length a - 1)
      in DA.array bounds [ (i, a !! i) | i <- [0..length a - 1] ]
    toArrayMat a = let rowLen = length (a !! 0)
                       bounds = ((0,0), (rowLen - 1, rowLen - 1))
                       merged = concat a
      in DA.array bounds [ (i, merged !! DA.index bounds i) | i <- DA.range bounds ]

arrayUnboxedTestCases = map f matAsListOfListCases
  where
    f (exp, mat, vec) = (toArrayVec exp, toArrayMat mat, toArrayVec vec)
    toArrayVec a = let bounds = (0, length a - 1)
      in DAU.array bounds [ (i, a !! i) | i <- [0..length a - 1] ] :: DAU.UArray Int Double
    toArrayMat a = let rowLen = length (a !! 0)
                       bounds = ((0,0), (rowLen - 1, rowLen - 1))
                       merged = concat a
      in DAU.array bounds [ (i, merged !! DAU.index bounds i) | i <- DAU.range bounds ] :: DAU.UArray (Int, Int) Double

vectorAsVecOfVecBoxedTestCases :: [(V.Vector Double, V.Vector (V.Vector Double), V.Vector Double)]
vectorAsVecOfVecBoxedTestCases = map toVector matAsListOfListCases
  where
    toVector (exp, mat, vec) = (V.fromList exp, V.fromList $ fmap V.fromList mat, V.fromList vec)

matvecList_returns_right_result :: Test
matvecList_returns_right_result = TestList $ map template matAsListOfListCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? matvecList mat vec

matvecListPar_returns_right_result :: Test
matvecListPar_returns_right_result = TestList $ map template matAsListOfListCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? matvecListPar mat vec

matvecListOne_returns_right_result :: Test
matvecListOne_returns_right_result = TestList $ map template matAsAListCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? matvecListOne mat vec

matvecListOnePar_returns_right_result :: Test
matvecListOnePar_returns_right_result = TestList $ map template matAsAListCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? matvecListOnePar mat vec

matvecVector_returns_right_result :: Test
matvecVector_returns_right_result = TestLabel "vector" $ TestList $ map template vectorAsOneVectorBoxedTestCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? matvecAsOneVectorBoxed mat vec

matvecAsVecOfVecBoxed_returns_right_results :: Test
matvecAsVecOfVecBoxed_returns_right_results = TestLabel "vector of vectors" $ TestList $map template vectorAsVecOfVecBoxedTestCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? matvecAsVecOfVecBoxed mat vec

arrayBoxed_returns_right_results :: Test
arrayBoxed_returns_right_results = TestLabel "array boxed" $ TestList $ map template arrayBoxedTestCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? MA.matvecPar mat vec

arrayUnboxed_returns_right_results :: Test
arrayUnboxed_returns_right_results = TestLabel "array boxed" $ TestList $ map template arrayUnboxedTestCases
  where
    template (exp, mat, vec) = TestCase $ exp @=? MAU.matvecPar mat vec

matvecTests =
    [ matvecList_returns_right_result
    , matvecListPar_returns_right_result
    , matvecListOne_returns_right_result
    , matvecListOnePar_returns_right_result
    , matvecVector_returns_right_result
    , matvecAsVecOfVecBoxed_returns_right_results
    , arrayBoxed_returns_right_results
    , arrayUnboxed_returns_right_results
    ]
