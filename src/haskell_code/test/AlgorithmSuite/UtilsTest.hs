module AlgorithmSuite.UtilsTest (
  utilsTests
) where

import           Algorithm.Paralell.Matmult.Vector        as MMV
import           Algorithm.Paralell.Matmult.VectorUnboxed as MUV
import           Data.List                                as L
import qualified Data.Vector                              as V
import qualified Data.Vector.Unboxed                      as VU
import           Test.HUnit
import           Test.HUnit.Base

transposeCases :: [(V.Vector (V.Vector Double), V.Vector (V.Vector Double))]
transposeCases
  = [
      (V.fromList $ map (V.fromList) [[0,1],[2,3]], V.fromList $ map (V.fromList) [[0,2],[1,3]])
    , (V.fromList $ map (V.fromList) [[0,1,3],[4,5,6],[7,8,9]], V.fromList $ map (V.fromList) [[0,4,7],[1,5,8],[3,6,9]])
    , (V.fromList $ map (V.fromList) [[0,1,2,3],[4,5,6,7],[8,9,10,11],[12,13,14,15]], V.fromList $ map (V.fromList) [[0,4,8,12],[1,5,9,13],[2,6,10,14],[3,7,11,15]])
    ]

transposeUnboxCases :: [(V.Vector (VU.Vector Double), V.Vector (VU.Vector Double))]
transposeUnboxCases
  = map unbox transposeCases
    where
    unboxCase = V.map $ V.convert
    unbox (a,b) = (unboxCase a, unboxCase b)

testTranspose = TestLabel "transpose" $ TestList $ map template transposeCases
  where template (inp, exp) = TestCase $ MMV.transposeV inp @?= exp

testTransposeUnbox = TestLabel "transpose unboxed" $ TestList $ map template transposeUnboxCases
  where template (inp, exp) = TestCase $ MUV.transposeV inp @?= exp

utilsTests = [ testTranspose, testTransposeUnbox ]

