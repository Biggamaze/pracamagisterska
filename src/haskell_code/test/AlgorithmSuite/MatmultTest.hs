module AlgorithmSuite.MatmultTest where

import qualified Algorithm.Paralell.Matmult.Array         as MA
import qualified Algorithm.Paralell.Matmult.ArrayUnboxed  as MAU
import           Algorithm.Paralell.Matmult.List
import qualified Algorithm.Paralell.Matmult.Vector        as AV
import qualified Algorithm.Paralell.Matmult.VectorUnboxed as AVU
import qualified Data.Array                               as DA
import qualified Data.Array.Unboxed                       as DAU
import           Data.List
import qualified Data.Vector                              as V
import qualified Data.Vector.Unboxed                      as VU
import           Test.HUnit
import           Test.HUnit.Base

matmultTests
  = [ matmultList_resturns_right_result
    , matmultListPar_resturns_right_result
    , matmultListParChunks_returns_right_result
    , matmult_Vector_tests
    , matmultPar_Vector_tests
    , matmult_VectorUnbox_tests
    , matmultPar_VectorUnbox_tests
    , arrayBoxed_tests
    , arrayUnboxed_tests
    ]

matmultList_resturns_right_result = TestLabel "list" $ TestList $ map template testCases
  where template (lmat, umat, res) = TestCase $ matmultList lmat umat @?= res

matmultListPar_resturns_right_result = TestLabel "list" $ TestList $ map template testCases
  where template (lmat, umat, res) = TestCase $ matmultListPar lmat umat @?= res

matmultListParChunks_returns_right_result = TestLabel "list" $ TestList $ map template testCases
  where template (lmat, umat, res) = TestList $ map (withSize lmat umat res) [1, 2, 3]
        withSize l u r size = TestCase $ matmultListParChunks size l u @?= r

matmult_Vector_tests = TestLabel "vector boxed" $ TestList $ map template vectorCases
  where template (a, b, c) = TestCase $ AV.matmult a b @?= c

matmultPar_Vector_tests = TestLabel "vector boxed" $ TestList $ map template vectorCases
  where template (a, b, c) = TestCase $ AV.matmultPar a b @?= c

matmult_VectorUnbox_tests = TestLabel "vector boxed" $ TestList $ map template vectorUnboxCases
  where template (a, b, c) = TestCase $ AVU.matmult a b @?= c

matmultPar_VectorUnbox_tests = TestLabel "vector boxed" $ TestList $ map template vectorUnboxCases
  where template (a, b, c) = TestCase $ AVU.matmultPar a b @?= c

arrayBoxed_tests = TestLabel "array boxed" $ TestList $ map t arrayBoxedTestCases
  where t (a, b, c) = TestCase $ MA.matmult a b @?= c

arrayUnboxed_tests = TestLabel "array unboxed" $ TestList $ map t arrayUnboxedTestCases
  where t (a, b, c) = TestCase $ MAU.matmult a b @?= c

testCases :: [([[Double]], [[Double]], [[Double]])]
testCases =
  [ ([[1, 0], [0, 1]], [[1, 0], [0, 1]], [[1, 0], [0, 1]])
  , ([[2, 1, 3], [-1, 4, 0],[1,1,1]], [[1, 3, 2], [-2, 0, 1], [5, -3, 2]], [[15, -3, 11], [-9, -3, 2],[4,0,5]])
  , ([[0]], [[0]], [[0]])
  , ([[1]], [[1]], [[1]])
  , ([[1,8]
     ,[2,9]],[[7,5]
             ,[6,8]],[[55,69]
                     ,[68,82]]
    )
  ]

type VectorMatrix = V.Vector (V.Vector Double)
vectorCases :: [(VectorMatrix, VectorMatrix, VectorMatrix)]
vectorCases = map toVectorCases testCases
  where toVectorCase lc = V.fromList $ map V.fromList lc
        toVectorCases (a,b,c) = (toVectorCase a, toVectorCase b, toVectorCase c)

type VectorMatrixUnboxed = V.Vector (VU.Vector Double)
vectorUnboxCases :: [(VectorMatrixUnboxed, VectorMatrixUnboxed, VectorMatrixUnboxed)]
vectorUnboxCases = map toVectorCases testCases
  where toVectorCase lc = V.fromList $ map VU.fromList lc
        toVectorCases (a,b,c) = (toVectorCase a, toVectorCase b, toVectorCase c)

arrayBoxedTestCases = map f testCases
  where
    f (exp, mat, vec) = (toArray exp, toArray mat, toArray vec)
    toArray a = let rowLen = length (a !! 0)
                    bounds = ((0,0), (rowLen - 1, rowLen - 1))
                    merged = concat a
      in DA.array bounds [ (i, merged !! DA.index bounds i) | i <- DA.range bounds ]

arrayUnboxedTestCases = map f testCases
  where
    f (exp, mat, vec) = (toArray exp, toArray mat, toArray vec)
    toArray a = let rowLen = length (a !! 0)
                    bounds = ((0,0), (rowLen - 1, rowLen - 1))
                    merged = concat a
      in DAU.array bounds [ (i, merged !! DAU.index bounds i) | i <- DAU.range bounds ] :: DAU.UArray (Int, Int) Double
