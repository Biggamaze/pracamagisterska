module Main where

import Test.Hspec
import Algorithm.Paralell.Functions

main :: IO ()
main = hspec $ do
    describe "Algorithm.Paralell.Functions.parMap" $ do
        it "works as map for empty list" $ do
            (map id [] :: [Int]) `shouldBe` parMap id []

    describe "Algorithm.Paralell.Functions.parMap" $ do
        it "works as map for singleton list" $ do
            (map id [1] :: [Int]) `shouldBe` parMap id [1]

    describe "Algorithm.Paralell.Functions.parMap" $ do
        it "works as map for multiple element list" $ do
            (map id [1,23] :: [Int]) `shouldBe` parMap id [1,23]

