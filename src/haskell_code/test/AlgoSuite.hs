module Main where

import           Control.Monad
import           Test.HUnit
import           Test.HUnit.Base

import           AlgorithmSuite.MatmultTest
import           AlgorithmSuite.MatvecTest
import           AlgorithmSuite.UtilsTest

main :: IO ()
main = do
  mapM_ runTestTT $ matvecTests
  mapM_ runTestTT $ matmultTests
  mapM_ runTestTT $ utilsTests
  return ()
