module Utils.RandomDataGeneration where

import           Data.List
import qualified Data.Vector         as V
import qualified Data.Vector.Unboxed as VU
import           System.Random

matOfDimension :: Int -> IO [[Double]]
matOfDimension n = do
  g <- getStdGen
  return $ replicate n $ take n (randoms g)

listOfLength :: Int -> IO [Double]
listOfLength n = do
  g <- getStdGen
  return $ take n (randoms g)

--vecBoxedOfLength :: Int -> V.Vector Double
--vecBoxedOfLength n = V.enumFromN 0 n
--
--vecUnboxedOfLength :: Int -> VU.Vector Double
--vecUnboxedOfLength n = VU.enumFromN 0 n
