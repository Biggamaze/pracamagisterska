module Utils.Helpers (
  parseDimensions
) where

import           Data.List
import           Text.Read

parseDimensions :: String -> Maybe [Int]
parseDimensions args = sequence . fmap readMaybe $ words [ f c | c <- args ]
  where f c
          | elem c ";," = ' '
          | otherwise   = c
