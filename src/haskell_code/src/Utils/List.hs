module Utils.List where

import Data.List

{-# INLINE toChunks #-}
toChunks :: Int -> [a] -> [[a]]
toChunks _ [] = []
toChunks size list
  = let (i, f) = splitAt size list
    in i : (toChunks size f)
