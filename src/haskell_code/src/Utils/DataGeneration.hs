{-# LANGUAGE FlexibleInstances #-}
module Utils.DataGeneration (
  matAsOneVectorBoxed
, vecAsVectorBoxed
, matAsVecOfVecBoxed
, matAsOneVectorUnboxed
, matAsVecOfVecUnboxed
, vecAsVectorUnboxed
, matAsOneList
, matAsListOfLists
, vecAsList
, matArray
, matUArray
, vecUArray
, vecArray
) where

import           Control.DeepSeq
import qualified Data.Array          as A
import qualified Data.Array.Unboxed  as AU
import qualified Data.Vector         as V
import qualified Data.Vector.Unboxed as VU

matAsOneVectorBoxed :: Int -> V.Vector Double
matAsOneVectorBoxed n = force $ V.enumFromN 0 (n*n)

matAsVecOfVecBoxed :: Int -> V.Vector (V.Vector Double)
matAsVecOfVecBoxed n = force $ V.fromList rows
  where rows = replicate n (vecBoxedOfLength n)

vecAsVectorBoxed :: Int -> V.Vector Double
vecAsVectorBoxed n = force $ vecBoxedOfLength n



matAsVecOfVecUnboxed :: Int -> V.Vector (VU.Vector Double)
matAsVecOfVecUnboxed n = force $ V.fromList rows
  where rows = replicate n (vecUnboxedOfLength n)

matAsOneVectorUnboxed :: Int -> VU.Vector Double
matAsOneVectorUnboxed n = force $ VU.enumFromN 0 (n*n)

vecAsVectorUnboxed :: Int -> VU.Vector Double
vecAsVectorUnboxed n = force $ vecUnboxedOfLength n



matAsOneList :: Int -> [Double]
matAsOneList n = force $ listOfLength (n*n)

matAsListOfLists :: Int -> [[Double]]
matAsListOfLists n = force $ replicate n row
  where row = listOfLength n

vecAsList :: Int -> [Double]
vecAsList n = force $ listOfLength n


-- Arrays

matArray :: Int-> A.Array (Int, Int) Double
matArray n = A.listArray ((0, 0),(n - 1, n - 1)) $ listOfLength (n*n)

vecArray :: Int-> A.Array Int Double
vecArray n = A.listArray (0, n - 1) $ listOfLength n

matUArray :: Int-> AU.UArray (Int, Int) Double
matUArray n = AU.listArray ((0, 0),(n - 1, n - 1)) $ listOfLength (n*n)

vecUArray :: Int-> AU.UArray Int Double
vecUArray n = AU.listArray (0, n - 1) $ listOfLength n

--instance (AU.Ix i, Num a) =>  NFData (AU.UArray (i, i) a) where
--  rnf a = a `seq` ()

instance (AU.Ix i, Num a) =>  NFData (AU.UArray i a) where
  rnf a = a `seq` ()

-- private

listOfLength :: Int -> [Double]
listOfLength n = take n . iterate (+1) $ 0

vecBoxedOfLength :: Int -> V.Vector Double
vecBoxedOfLength n = V.enumFromN 0 n

vecUnboxedOfLength :: Int -> VU.Vector Double
vecUnboxedOfLength n = VU.enumFromN 0 n
