module Algorithm.Paralell.Integration where
  
import Control.DeepSeq
import Control.Parallel.Strategies
import Control.Parallel
import GHC.Conc ( numCapabilities )

integrateT :: (RealFrac a, Enum a, NFData a) => (a -> a) -> (a,a) -> a -> a 
integrateT f (ini, fin) dx 
  = let lst = map f [ini,ini+dx..fin]
    in sum lst * dx - 0.5 * (f ini + f fin) * dx

integrateT_strategy :: (RealFrac a, Enum a, NFData a) => (a -> a) -> (a,a) -> a -> a
integrateT_strategy f (l,r) dx
  = let chunks = [ integrateT f (l + i*wChunk, l + (i+1)*wChunk) dx
                 | i<-[0..nChunks-1] ]
                 `using` parList rdeepseq
    in sum chunks
       where nChunks = fromIntegral numCapabilities
             wChunk = (r-l)/nChunks

integrateT_par :: (RealFrac a, Enum a, NFData a) => (a -> a) -> (a,a) -> a -> a
integrateT_par f (l,r) dx 
  = let chunks = [ force (integrateT f (l + i*wChunk, l + (i+1)*wChunk) dx) `par` (integrateT f (l + i*wChunk, l + (i+1)*wChunk) dx)
                 | i<-[0..nChunks-1] ]
    in sum chunks
        where nChunks = 100
              wChunk = (r-l)/nChunks
