module Algorithm.Paralell.Matvec.List where

import           Algorithm.Paralell.Functions (parMap)
import           Control.DeepSeq              (NFData)
import           Control.Parallel.Strategies  hiding (parMap)

{- Straightforward implementation with matrix modelled as list of lists -}
matvecList :: [[Double]] -> [Double] -> [Double]
matvecList mat vec = map (sum . zipWith (*) vec) mat

matvecListPar :: [[Double]] -> [Double] -> [Double]
matvecListPar mat vec = parMap (sum . zipWith (*) vec) mat

matvecListStrategy :: [[Double]] -> [Double] -> [Double]
matvecListStrategy mat vec = (matvecList mat vec) `using` (parTraversable rdeepseq)

{- Reference implementation with matrix modelled as one list -}
matvecListOne :: [Double] -> [Double] -> [Double]
matvecListOne mat vec = map (sum . zipWith (*) vec) (chunks mat)
  where
    chunks [] = []
    chunks m =
      let (init, rest) = splitAt (length vec) m
       in init : chunks rest

matvecListOnePar :: [Double] -> [Double] -> [Double]
matvecListOnePar mat vec = parMap (sum . zipWith (*) vec) (chunks mat)
  where
    chunks [] = []
    chunks m =
      let (init, rest) = splitAt (length vec) m
       in init : chunks rest

matvecListOneStrategy :: [Double] -> [Double] -> [Double]
matvecListOneStrategy mat vec = (matvecListOne mat vec) `using` (parTraversable rdeepseq)