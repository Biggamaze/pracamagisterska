module Algorithm.Paralell.Matvec.ArrayUnboxed (
  matvec
, matvecPar
) where

import           Data.Array.Unboxed
import           Control.Parallel.Strategies

matvec :: UArray (Int,Int) Double -> UArray Int Double -> UArray Int Double
matvec mat vec
  | jm /= jv || jm' /= jv' = error "range mismatch"
  | otherwise              = array (im, im') values
  where
  ((im,jm),(im', jm')) = bounds mat
  (jv, jv')            = bounds vec
  vr = range (jv, jv')
  mr = range (im, im')
  values = [ (i, sum [ mat ! (i, j) * vec ! j | j <- vr] ) | i <- mr ]

matvecPar :: UArray (Int,Int) Double -> UArray Int Double -> UArray Int Double
matvecPar mat vec
  | jm /= jv || jm' /= jv' = error "range mismatch"
  | otherwise              = array (im, im') values
  where
  ((im,jm),(im', jm')) = bounds mat
  (jv, jv')            = bounds vec
  vr = range (jv, jv')
  mr = range (im, im')
  values = [ (i, sum [ mat ! (i, j) * vec ! j | j <- vr] ) | i <- mr ] `using` parList rdeepseq