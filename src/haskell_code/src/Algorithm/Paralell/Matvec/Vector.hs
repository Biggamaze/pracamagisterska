module Algorithm.Paralell.Matvec.Vector (
  matvecAsOneVectorBoxed
, matvecParAsOneVectorBoxed
, matvecAsVecOfVecBoxed
, matvecParAsVecOfVecBoxed
, matvecAsVecOfVecUnboxed
, matvecParAsVecOfVecUnboxed
, matvecParAsVecOfVecBoxedWithCapabilities
) where

import           Control.Parallel
import           Control.Parallel.Strategies
import qualified Data.Vector                 as V
import qualified Data.Vector.Unboxed         as VU
 

matvecAsOneVectorBoxed :: V.Vector Double -> V.Vector Double -> V.Vector Double
matvecAsOneVectorBoxed mat vec = V.map multAndSum rows
  where n = V.length vec
        multAndSum rowStart = V.sum . V.zipWith (*) vec $ (V.slice rowStart n mat)
        rows = V.iterateN n (+n) 0

matvecAsVecOfVecBoxed :: V.Vector (V.Vector Double) -> V.Vector Double -> V.Vector Double
matvecAsVecOfVecBoxed mat vec = V.map multAndSum mat
  where multAndSum = V.sum . V.zipWith (*) vec



matvecParAsOneVectorBoxed :: V.Vector Double -> V.Vector Double -> V.Vector Double
matvecParAsOneVectorBoxed mat vec = vecPar
  where vector = matvecAsOneVectorBoxed mat vec
        half = (V.length vector) `div` 2
        v1 = V.take half vector
        v2 = V.drop half vector
        vecPar = v1 `par` v2 `pseq` V.concat [v1, v2]

matvecParAsVecOfVecBoxed :: V.Vector (V.Vector Double)
                            -> V.Vector Double
                            -> V.Vector Double
matvecParAsVecOfVecBoxed mat vec = matvecAsVecOfVecBoxed mat vec
                                   `using` parTraversable rdeepseq


matvecParAsVecOfVecBoxedWithCapabilities :: Int -> V.Vector (V.Vector Double) -> V.Vector Double -> V.Vector Double
matvecParAsVecOfVecBoxedWithCapabilities capabilities mat vec 
  = let multAndSum vec   = V.sum . V.zipWith (*) vec
        chunks = toChunks (V.length mat `quot` capabilities) mat
        
        chunkApply :: [V.Vector (V.Vector Double)] -> V.Vector Double -> [V.Vector Double]
        chunkApply []     _   = []
        chunkApply (a:as) vec = let i = V.map (multAndSum vec) a
                                    j = chunkApply as vec
                                in i `par` j `pseq` i : j
    in V.concat (chunkApply chunks vec)

matvecAsVecOfVecUnboxed :: V.Vector (VU.Vector Double) -> VU.Vector Double -> V.Vector Double
matvecAsVecOfVecUnboxed mat vec = V.map multAndSum mat
  where multAndSum = VU.sum . VU.zipWith (*) vec

matvecParAsVecOfVecUnboxed :: V.Vector (VU.Vector Double) -> VU.Vector Double -> V.Vector Double
matvecParAsVecOfVecUnboxed mat vec = matvecAsVecOfVecUnboxed mat vec `using` parTraversable rdeepseq



toChunks :: Int -> V.Vector (V.Vector Double) -> [V.Vector (V.Vector Double)]
toChunks size vector
  | V.null vector = []
  | otherwise     = beg : (toChunks size rem)
  where (beg, rem) = V.splitAt size vector
