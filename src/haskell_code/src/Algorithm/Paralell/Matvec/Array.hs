module Algorithm.Paralell.Matvec.Array (
  matvec
, matvecPar
) where

import           Data.Array
import           Control.Parallel.Strategies

matvec :: Array (Int,Int) Double -> Array Int Double -> Array Int Double
matvec mat vec
  | jm /= jv || jm' /= jv' = error "range mismatch"
  | otherwise              = array (im, im') values
  where
  ((im,jm),(im', jm')) = bounds mat
  (jv, jv')            = bounds vec
  vr = range (jv, jv')
  mr = range (im, im')
  values = [ (i, sum [ mat ! (i, j) * vec ! j | j <- vr] ) | i <- mr ]

matvecPar :: Array (Int,Int) Double -> Array Int Double -> Array Int Double
matvecPar mat vec = matvec mat vec `using` parTraversable rdeepseq
