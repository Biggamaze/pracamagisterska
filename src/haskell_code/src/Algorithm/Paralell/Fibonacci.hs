module Algorithm.Paralell.Fibonacci (
    fib
  , fibPar
  , fibParThreshold
) where

import Control.Parallel

{-
    Calculates n-th element of Fibonacci's series defined as:
    f(0) = 1
    f(1) = 1
    f(n) = f(n-1) + f(n-2)
-}

fib :: (Num a, Ord a) => a -> a
fib n 
    | n <= 1 = 1
    | otherwise = fib (n-1) + fib (n-2)

fibPar :: (Num a, Ord a) => a -> a
fibPar n 
    | n <= 1 = 1
    | otherwise = let f1 = fibPar (n-1) 
                      f2 = fibPar (n-2)
                  in f1 `par` f2 `pseq` f1 + f2

fibParThreshold :: (Num a, Ord a) => a -> a -> a
fibParThreshold n threshold
    | n <= 1  = 1
    | n <= threshold = fib n
    | otherwise = let f1 = fibParThreshold (n-1) threshold 
                      f2 = fibParThreshold (n-2) threshold
                  in f1 `par` f2 `pseq` f1 + f2
                  