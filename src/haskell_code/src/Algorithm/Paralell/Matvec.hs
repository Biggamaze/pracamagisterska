{-# LANGUAGE RankNTypes #-}
module Algorithm.Paralell.Matvec 
  ( sequentialMatVec
  , parallelMatVec
  ) where

import Data.List
import Control.DeepSeq
import qualified Data.Vector as V
import Control.Parallel
import Control.Concurrent
import System.Clock
import Algorithm.Paralell.Functions
import Algorithm.Paralell.Matvec.Vector

sequentialMatVec :: Int -> IO (Double, Double)
sequentialMatVec n = evaluator n matvecAsOneVectorBoxed

parallelMatVec :: Int -> IO (Double, Double)
parallelMatVec n = evaluator n matvecParAsOneVectorBoxed

type MatVecFunc a = V.Vector a -> V.Vector a -> V.Vector a

{-
    Helpers
-}

evaluator :: Int -> MatVecFunc Double -> IO (Double, Double)
evaluator n func = do
    let mat = V.enumFromN 0 (n*n) :: V.Vector Double
    let vec = V.enumFromN 0 n :: V.Vector Double 

    time1 <- getTime Monotonic
    time2 <- mat `deepseq` vec `deepseq` (getTime Monotonic)

    time3 <- getTime Monotonic
    let res = func mat vec
    time4 <- res `deepseq` (getTime Monotonic)
    
    let evalTime = ((toDouble time2) - (toDouble time1)) / (10^9) 
    let calcTime = ((toDouble time4) - (toDouble time3)) / (10^9)

    return (force evalTime, force calcTime)

toDouble :: TimeSpec -> Double
toDouble ts = fromIntegral (toNanoSecs ts)