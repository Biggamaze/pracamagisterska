module Algorithm.Paralell.Matmult.Vector (
  matmult
, matmultPar
, transposeV
) where

import           Control.Parallel.Strategies
import qualified Data.Vector                 as V

matmult :: V.Vector (V.Vector Double) -> V.Vector (V.Vector Double) -> V.Vector (V.Vector Double)
matmult a b = V.map f a
  where f row = V.map (V.sum . V.zipWith (*) row) b'
        b' = transposeV b

matmultPar :: V.Vector (V.Vector Double) -> V.Vector (V.Vector Double) -> V.Vector (V.Vector Double)
matmultPar a b = matmult a b `using` parTraversable rdeepseq

transposeV :: V.Vector (V.Vector Double) -> V.Vector (V.Vector Double)
transposeV mat = V.fromList $ columns
  where n = V.length (mat V.! 0)
        columns = map (\c -> V.map (\v -> v V.! c) mat) [0..n-1]
