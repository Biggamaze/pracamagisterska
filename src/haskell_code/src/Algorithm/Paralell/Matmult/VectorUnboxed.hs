{-# LANGUAGE FlexibleContexts #-}
module Algorithm.Paralell.Matmult.VectorUnboxed (
  matmult
, matmultPar
, transposeV
) where

import           Control.Parallel.Strategies
import qualified Data.Vector                 as V
import qualified Data.Vector.Unboxed         as VU

matmult :: V.Vector (VU.Vector Double) -> V.Vector (VU.Vector Double) -> V.Vector (VU.Vector Double)
matmult a b = V.map f a
  where f row = V.convert . V.map (VU.sum . VU.zipWith (*) row) $ b'
        b' = transposeV b

matmultPar :: V.Vector (VU.Vector Double) -> V.Vector (VU.Vector Double) -> V.Vector (VU.Vector Double)
matmultPar a b = matmult a b `using` parTraversable rdeepseq

-- might be painful due to this `convert`
transposeV :: V.Vector (VU.Vector Double) -> V.Vector (VU.Vector Double)
transposeV mat = V.fromList $ columns
  where n = VU.length (mat V.! 0)
        columns = map (\c -> V.convert $ V.map (\v -> v VU.! c) mat) [0..n-1]
