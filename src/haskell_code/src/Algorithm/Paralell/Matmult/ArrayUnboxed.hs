module Algorithm.Paralell.Matmult.ArrayUnboxed (
  matmult
, matmultPar
) where

import           Data.Array.Unboxed
import           Control.Parallel.Strategies

matmult :: UArray (Int,Int) Double -> UArray (Int,Int) Double -> UArray (Int,Int) Double
matmult x y
  | x1 /= y0 || x1' /= y0'  = error "range mismatch"
  | otherwise               = array ((x0,y1),(x0',y1')) l
  where
    ((x0,x1),(x0',x1')) = bounds x
    ((y0,y1),(y0',y1')) = bounds y
    ir = range (x0,x0')
    jr = range (y1,y1')
    kr = range (x1,x1')
    l  = [((i,j), sum [x!(i,k) * y!(k,j) | k <- kr]) | i <- ir, j <- jr]

matmultPar :: UArray (Int,Int) Double -> UArray (Int,Int) Double -> UArray (Int,Int) Double
matmultPar x y
  | x1 /= y0 || x1' /= y0'  = error "range mismatch"
  | otherwise               = array ((x0,y1),(x0',y1')) l
  where
    ((x0,x1),(x0',x1')) = bounds x
    ((y0,y1),(y0',y1')) = bounds y
    ir = range (x0,x0')
    jr = range (y1,y1')
    kr = range (x1,x1')
    l  = [((i,j), sum ([x!(i,k) * y!(k,j) | k <- kr])) | i <- ir, j <- jr] `using` parList rdeepseq