module Algorithm.Paralell.Matmult.List 
  ( matmult
  , matmultPar
  , matmultList
  , matmultListPar
  , matmultListStrategy
  , matmultListParChunks
  , Mat
  ) where

import           Algorithm.Paralell.Functions
import           Control.DeepSeq
import           Control.Parallel.Strategies  hiding (parMap)
import           Data.List
import qualified Data.Vector                  as V
import           Utils.List

type Mat a = V.Vector (V.Vector Double)

matmult :: Mat Double -> Mat Double -> Mat Double
matmult a b = fmap multiplyRowByEachColumn a
  where
    multiplyRowByEachColumn r = fmap (\c -> V.sum . V.zipWith (*) r $ c) $ b

matmultPar :: Mat Double -> Mat Double -> Mat Double
matmultPar a b = (matmult a b) `using` parTraversable rdeepseq

matmultList :: [[Double]] -> [[Double]] -> [[Double]]
matmultList a b = fmap multiplyRowByEachColumn a
  where multiplyRowByEachColumn r = fmap (\c -> sum $ zipWith (*) r c) $ b'
        b' = transpose b

matmultListStrategy :: [[Double]] -> [[Double]] -> [[Double]]
matmultListStrategy a b = matmultList a b `using` parList rdeepseq

matmultListPar :: [[Double]] -> [[Double]] -> [[Double]]
matmultListPar a b = parMap multiplyRowByEachColumn a
  where multiplyRowByEachColumn r = map (\c -> sum . zipWith (*) r $ c) $ b'
        b' = transpose b

matmultListParChunks :: Int -> [[Double]] -> [[Double]] -> [[Double]]
matmultListParChunks size a b
  = let
        chunks = toChunks size a
        b' = transpose b
    in concat $ parMap (flip matmultList $ b) chunks