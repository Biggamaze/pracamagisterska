module Algorithm.Paralell.Matmult.Array (
  matmult
, matmultStrategy
) where

import           Data.Array
import           Control.Parallel.Strategies

matmult :: Array (Int,Int) Double -> Array (Int,Int) Double -> Array (Int,Int) Double
matmult x y
  | x1 /= y0 || x1' /= y0'  = error "range mismatch"
  | otherwise               = array ((x0,y1),(x0',y1')) l
  where
    ((x0,x1),(x0',x1')) = bounds x
    ((y0,y1),(y0',y1')) = bounds y
    ir = range (x0,x0')
    jr = range (y1,y1')
    kr = range (x1,x1')
    l  = [((i,j), sum [x!(i,k) * y!(k,j) | k <- kr]) | i <- ir, j <- jr]

matmultPar :: Array (Int,Int) Double -> Array (Int,Int) Double -> Array (Int,Int) Double
matmultPar x y
  | x1 /= y0 || x1' /= y0'  = error "range mismatch"
  | otherwise               = array ((x0,y1),(x0',y1')) q
  where
    ((x0,x1),(x0',x1')) = bounds x
    ((y0,y1),(y0',y1')) = bounds y
    ir = range (x0,x0')
    jr = range (y1,y1')
    kr = range (x1,x1')
    q  = do i <- ir
            j <- jr
            return ((i,j), sum ([(x!(i,k) * y!(k,j)) | k <- kr] `using` parList rdeepseq))

matmultStrategy :: Array (Int,Int) Double -> Array (Int,Int) Double -> Array (Int,Int) Double
matmultStrategy x y = matmult x y `using` parTraversable rdeepseq