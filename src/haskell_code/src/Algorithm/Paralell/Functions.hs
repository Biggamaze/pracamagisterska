module Algorithm.Paralell.Functions where

import           Control.DeepSeq
import           Control.Parallel

{-# INLINE parMap #-}
parMap :: NFData b => (a -> b) -> [a] -> [b]
parMap _ [] = []
parMap f (a:as) =
  let v = f a
      vs = parMap f as
  in rnf v `par` vs `pseq` v : vs
