{-# LANGUAGE OverloadedStrings #-}

module Algorithm.Concurrent.PubSTM where

import           Algorithm.Concurrent.Utility
import           Control.Concurrent
import           Control.Concurrent.Chan
import           Control.Concurrent.MVar
import           Control.Concurrent.STM
import           Control.Monad
import           Control.Monad.Trans
import           Control.Monad.Writer
import           Data.Text                    hiding (reverse)
import           Prelude
import           System.IO

type Mugs = TVar Int

type Tap = TMVar ()

data JobParameters = JobParameters
  { channel :: Chan String
  , mugs    :: Mugs
  , tap     :: Tap
  , beers   :: Int
  }

fillDelay = 1 * 10 ^ 6 --us

drinkDelay = 5 * 10 ^ 6 --us

openPub :: (Text -> IO ()) -> Int -> Int -> IO ()
openPub logger numClients numMugs = do
  hSetBuffering stdout NoBuffering
  ch <- newChan
  forkIO $ concurrentWriter ch logger
  mugs <- atomically $ newTVar numMugs
  tap <- atomically $ newTMVar ()
  vars <- forM [0 .. numClients - 1] $ \c -> forkThread $ clientJob ch mugs tap
  waitWithChan vars ch
  writeChan ch "Finished simulation"

clientJob :: Chan Text -> Mugs -> Tap -> IO ()
clientJob ch mugs tap = do
  myId <- myThreadId
  forM_ (reverse [1 .. 5]) $ \l -> do
    writeChan ch $ flip logWithId myId $ pack ("Liters to go: " ++ show l)
    takeLog <- atomically . execWriterT . takeMug $ mugs
    forM_ takeLog $ \log -> writeChan ch $ logWithId log myId
    takeTapLog <- atomically . takeTap $ tap
    writeChan ch $ logWithId takeTapLog myId
    threadDelay fillDelay
    freeTapLog <- atomically . freeTap $ tap
    writeChan ch $ logWithId freeTapLog myId
    threadDelay drinkDelay
    putMugLog <- atomically . putMug $ mugs
    writeChan ch $ logWithId putMugLog myId
    return ()
  writeChan ch $ logWithId "Drunk all. Leaving" myId

takeMug :: Mugs -> WriterT [Text] STM ()
takeMug mugs = do
  availNum <- lift $ readTVar mugs
  if availNum > 0
    then do
      lift . writeTVar mugs $ (availNum - 1)
      tell [pack $ "Taking one mug out of " ++ show availNum]
    else do
      tell ["No free mugs. Waiting ..."] --will never be shown :)
      lift retry

putMug :: Mugs -> STM Text
putMug mugs = do
  modifyTVar' mugs (+ 1)
  return "Put mug back"

takeTap :: Tap -> STM Text
takeTap tap = do
  takeTMVar tap
  return "Filling the mug"

freeTap :: Tap -> STM Text
freeTap tap = do
  putTMVar tap ()
  return "Filled. Tap free"

concurrentWriter :: Chan Text -> (Text -> IO ()) -> IO ()
concurrentWriter ch f = do
  readChan ch >>= f
  concurrentWriter ch f
