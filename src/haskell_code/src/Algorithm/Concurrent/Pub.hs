module Algorithm.Concurrent.Pub where

import Algorithm.Concurrent.Utility
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Exception.Base
import Control.Monad

mugs = 3 :: Int

taps = 2 :: Int

type Mugs = MVar Int

type Liters = Int

type Refill = MVar Int -> IO ()

pubSimulation :: [String] -> IO ()
pubSimulation args = do
  vMug <- newMVar mugs
  tap <- newMVar taps
  putStrLn $ "Taps available: " ++ show taps
  putStrLn $ "Mugs available: " ++ show mugs
  vars <- forM [5 .. 7] $ \n ->  forkThread (clientJob (refill tap) vMug n)
  wait vars

refillTime = 100000

clientJob :: Refill -> Mugs -> Liters -> IO ThreadId
clientJob refillF vMugs lit =
  let drink curL
        | curL <= 0 = do
          id <- myThreadId
          putStrLn $ "Client " ++ show id ++ " drunk all. Leaving!"
          return id
        | otherwise = do
          id <- myThreadId
          putStrLn $ "Client " ++ show id ++ " attempting to take mug"
          var <- takeMVar vMugs
          if var > 0
            then do
              putMVar vMugs (var - 1)
              putStrLn $ "Client " ++ show id ++ " took mug .. " ++ show (var - 1) ++ " mugs available"
              putStrLn $
                "Client " ++
                show id ++ " drunk " ++ show (lit - curL + 1) ++ " liters - " ++ show (curL - 1) ++ " liters to go!"
              putStrLn $ "Client " ++ show id ++ " put mug back! "
              forkIO $ refillF vMugs
              drink (curL - 1)
            else do
              putStrLn $ "Client " ++ show id ++ " waiting for mug :("
              threadDelay refillTime
              putMVar vMugs var
              drink curL
   in do id <- myThreadId
         putStrLn $ "Client " ++ show id ++ " entered pub! Want to drink " ++ show lit ++ " liters!"
         drink lit

refill :: MVar Int -> Mugs -> IO ()
refill tapMV mugsMV = do
  taps <- takeMVar tapMV
  if taps <= 0
    then do
      putMVar tapMV taps
      putStrLn  "Not taps free for refill ... Waiting"
      threadDelay refillTime
    else do
      putMVar tapMV (taps - 1)
      putStrLn "Refilling mug ..."
      threadDelay refillTime
      modifyMVar_ tapMV (\n -> return (n + 1))
      modifyMVar_ mugsMV (\n -> return (n + 1))
      putStrLn "Refilled ..."