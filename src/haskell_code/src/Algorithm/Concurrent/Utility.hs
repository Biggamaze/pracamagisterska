{-# LANGUAGE OverloadedStrings #-}

module Algorithm.Concurrent.Utility where

import           Control.Concurrent
import           Control.Concurrent.Chan
import           Control.Concurrent.MVar
import           Control.Exception.Base
import           Control.Monad           (forM_)
import           Data.Text

forkThread action = do
  var <- newEmptyMVar
  forkFinally action (putMVar var)
  return var

wait vars =
  forM_ vars $ \v -> do
    val <- takeMVar v
    case val of
      (Right c) -> return ()
      (Left e)  -> putStrLn $ "Finished with error " ++ show e

waitWithChan vars ch =
  forM_ vars $ \v -> do
    val <- takeMVar v
    case val of
      (Right c) -> return ()
      (Left e)  -> writeChan ch $ pack $ "Finished with error " ++ show e

logWithId :: Text -> ThreadId -> Text
logWithId base id = pack $ show id ++ ": " ++ unpack base
