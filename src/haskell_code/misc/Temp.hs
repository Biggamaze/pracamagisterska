module Main where

import           Control.DeepSeq
import           Control.Monad
import           Control.Parallel.Strategies      hiding (parMap, parList, evalList)
import qualified Data.Vector                      as V
import           Algorithm.Paralell.Matmult.List
import           Algorithm.Paralell.Matvec.Vector
import           Algorithm.Paralell.Matvec.List
import           Utils.RandomDataGeneration       as DG
import           Control.Parallel                 (par, pseq)

main :: IO ()
main = do
  matBoxed1000 <- matOfDimension 1000
  matBoxed1000 `deepseq` return ()

  putStrLn $ show $ length matBoxed1000

  let res = matmultListParChunks 50 matBoxed1000 matBoxed1000

  putStrLn $ show $ length res

-- parList :: Strategy [a]
-- parList []     = return []
-- parList (x:xs) = do
--   xn  <- rpar x
--   xsn <- parList xs
--   return (xn:xsn)

parMap :: (a -> b) -> [a] -> [b]
parMap f []       = []
parMap f l@(x:xs) = map f l `using` parList

evalList :: Strategy a -> Strategy [a]
evalList elemStrat []       = return []
evalList elemStrat l@(x:xs) = do
  xn  <- elemStrat x
  xsn <- evalList elemStrat xs
  return (xn:xsn)

parList :: Strategy [a]
parList list = evalList rpar list

parListWith :: Strategy a -> Strategy [a]
parListWith strat list = evalList (rparWith strat) list

parListNF :: NFData a => Strategy [a]
parListNF list = parListWith rdeepseq list

parListWHNF :: Strategy [a]
parListWHNF list = parListWith rseq list